import dotenv from 'dotenv';
import config, {nodeEnv} from './config/config';
import express from 'express';
import usersRouter from './api/users';
import placesRouter from './api/places';
import postsRouter from './api/posts';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import {loadUsers} from './api/users/usersData';
import {loadPlaces} from './api/places/placesData';
import {loadPosts} from './api/posts/postsData';
import {Mockgoose} from 'mockgoose';
//const passport = require('passport');
// import passport configured with JWT strategy​
//import passport from './auth';​
// import passport configured with JWT strategy​
dotenv.config();
//server.use(passport.initialize());​
export const server = express();

// Connect to database
if (nodeEnv == 'test') {
	let mockgoose = new Mockgoose(mongoose);
	mockgoose.prepareStorage().then(function() {
  	  mongoose.connect(config.mongoDb);
	});
}else{
	mongoose.connect(config.mongoDb);
}

mongoose.connection.on('error', function(err) {
    console.error('MongoDB connection error: '+ err);
    process.exit(-1);
});
// Populate DB with sample data
if(config.seedDb) {
    loadUsers();
    loadPlaces();
    loadPosts();
}

// configure body-parser
server.use(bodyParser.json());
server.use(bodyParser.urlencoded());

// NEW (Start
server.use(function(req, res, next) {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');
    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 
       'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 
      'X-Requested-With,content-type');
    // Set to true if you need the website to 
    // include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);
    // Pass to next layer of middleware
    next();
});
// NEW (End)

// create api endpoints
server.use('/api/users', usersRouter);
server.use('/api/places', placesRouter);
server.use('/api/posts', postsRouter);
server.use(express.static('public'));


server.listen(config.port, () => {
  console.info('Your app is running on port:', config.port);
});
