# Assignment 2 - Express - mongoDB backend for the Amazing world React application


## Overview:

This is the full backend implementation for the React Amazing World application. It is fully connected to the front-end React part and gives a persistent storage through the mongoDB cloud database.


#### Application features are:


 + Full CRUD functionality for the Places 
 + User sign in and login in
 + User password hashed and salted
 + User authentication
 + Protection of certain routes from the non-signed-in user
 + Unit Testing using Mocha testing framework
 + Responsive header
 + Basic data validation 


#### Running instrunctions:


Open a window terminal and clone the application into your local folder then type following:

 `cd amazing-world-node-backend`


 `npm install`


 `npm start`


Open a second window terminal and navigate into the same local folder


`cd amazing-world-node-backend`

`cd amazing world`

`npm install`

`npm run dev`


In order to get a full functionality, you need to have the mongo db running, so in a third terminal:


`mongod`


This should start the mongoDB datatbase instance, running on port 27017 by default.



Open a browser of your choice, preferrable Chrome. Go to 
[Amazing world](http://localhost:3000/) home page and explore.



## API Design


| HTTP Verb & Path |  Description |
| -- | -- |
| GET: /api/places|return a list of places |
| POST: /api/places |add a new place |
| PUT: /api/places/:id | update a place |
| DELETE: /api/places/:id | delete a place |
| POST: /api/users?action=register | register a user |
| POST: /api/users | login user



## API Configuration


The .env file is located at the root of the folder, but it is excluded from the Git repo.
Additional node server configuration is in ./config/config.js file.

~~~bash
NODE_ENV=development
PORT=8080
HOST=localhost
mongoDB=YourMongoURL
seedDb=true
secret=ilikecake
~~~


## Security and Authentication

JWT User authentication by storing an encrypted user token in the local storage when the user is logged in.


## Testing

Unit testing of all backend api methods. Testing report awailable at mochawesome-report folder in the root of the application.

To run the unit tests open a terminal and navigate into your local folder, then:


`cd amazing-world-node-backend`

`npm run test`
![][image4]

## Extra features

+ User authentication through the JWT token that is stored in the local storage.
+ Routes protected from the unauthenticated access:
 ++ /userHome
+ Email validation

## Independent learning.

+ Redux framework, implemented 
+ Google API
+ Travis CI


[image4]: ./public/about/test/Test_Report.png
