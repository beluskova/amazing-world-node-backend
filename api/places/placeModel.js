const mongoose = require('mongoose'),
Schema = mongoose.Schema;

const PlaceSchema = new Schema({
  imageName: String,
  imageUrl: String,
  name: String,
  state: String, 
  posts: [],
  type: String,
  interesting: String,
  links: String,
  description: String

});

PlaceSchema.path('name').validate((v)=>{
	if(v.length>50||v.length<5){
		return false;
	}
	return true;
});


export default mongoose.model('Place', PlaceSchema);