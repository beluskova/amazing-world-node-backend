import mongoose from 'mongoose';
import assert from 'assert';
import placeModel from './placeModel';
import config from './../../config/config';

const places =[
    {
        "imageName": "canolaChina.jpg",
        "imageUrl": "../../public/assets/images/canola-flower-fields-china.jpg",
        "name": "Canola Flower Fields",
        "state": "China",
        "posts": [],
        "type": "created by humans",
        "interesting": "canola is rapeseed",
        "links": "http://www.visiontimes.com/2015/03/03/a-gorgeous-sea-of-yellow-the-canola-flower-fields-of-luoping-photos.html?photo=2",
        "desription": "Opened in October 2016, Anantara Al Jabal Al Akhdar Resort brings the Anantara brand to Oman. Resting on the breathtaking Green Mountain, the resort combines luxury and style with a backdrop of outstanding panoramic views of these fabled mountains."
      },
      {
        "imageName": "antelope-canyon-usa.jpg",
        "imageUrl": "/src/public/assets/images/antelope-canyon-usa.jpg",
        "name": "Antelope Canyon",
        "state": "USA",
        "posts": [],
        "type": "created by hueeeeemans",
        "interesting": "canola is rapeseed",
        "links": "http://www.visiontimes.com/2015/03/03/a-gorgeous-sea-of-yellow-the-canola-flower-fields-of-luoping-photos.html?photo=2",
        "description": "Antelope Canyon is a slot canyon in the American Southwest. It is located on Navajo land east of Page, Arizona. Antelope Canyon includes two separate, scenic slot canyon sections, referred to individually as Upper Antelope Canyon or The Crack; and Antelope Canyon or The Corkscrew."
      },
      {
        "imageName": "bamboo-forest-japan.jpg",
        "imageUrl": "../../public/assets/images/bamboo-forest-japan.jpg",
        "name": "Bamboo Forest",
        "state": "Japan",
        "posts": [],
        "type": "created by humans",
        "interesting": "canola is rapeseed",
        "links": "http://www.visiontimes.com/2015/03/03/a-gorgeous-sea-of-yellow-the-canola-flower-fields-of-luoping-photos.html?photo=2",
        "description": "Bamboo Forest is a tourist site in Arashiyama, Kyoto, Japan. On CNN, it was referred to as One of the most beautiful groves on Earth. The Ministry of the Environment included the Sagano Bamboo Forest on its list of 100 Soundscapes of Japan."
      },
      {
        "imageName": "black-forest-germany.jpg",
        "imageUrl": "../../public/assets/images/black-forest-germany.jpg",
        "name": "Black Forest",
        "state": "Germany",
        "posts": [],
        "type": "created by humans",
        "interesting": "canola is rapeseed",
        "links": "http://www.visiontimes.com/2015/03/03/a-gorgeous-sea-of-yellow-the-canola-flower-fields-of-luoping-photos.html?photo=2",
        "description": "The Black Forest stretches from the High Rhine in the south to the Kraichgau in the north. In the west it is bounded by the Upper Rhine Plain (which, from a natural region perspective, also includes the low chain of foothills); in the east it transitions to the Gäu, Baar and hill country west of the Klettgau. The Black Forest is the highest part of the South German Scarplands and much of it is densely wooded."
      },
      {
        "imageName": "bonn-germany.jpg",
        "imageUrl": "../../public/assets/images/canola-flower-fields-china.jpg",
        "name": "Canola Flower Fields",
        "state": "China",
        "posts": [],
        "type": "created by humans",
        "interesting": "canola is rapeseed",
        "links": "http://www.visiontimes.com/2015/03/03/a-gorgeous-sea-of-yellow-the-canola-flower-fields-of-luoping-photos.html?photo=2",
        "desription": "Opened in October 2016, Anantara Al Jabal Al Akhdar Resort brings the Anantara brand to Oman. Resting on the breathtaking Green Mountain, the resort combines luxury and style with a backdrop of outstanding panoramic views of these fabled mountains."
      },
      {
        "imageName": "hamilton-princess-bermuda.jpg",
        "imageUrl": "/src/public/assets/images/antelope-canyon-usa.jpg",
        "name": "Antelope Canyon",
        "state": "USA",
        "posts": [],
        "type": "created by hueeeeemans",
        "interesting": "canola is rapeseed",
        "links": "http://www.visiontimes.com/2015/03/03/a-gorgeous-sea-of-yellow-the-canola-flower-fields-of-luoping-photos.html?photo=2",
        "description": "Antelope Canyon is a slot canyon in the American Southwest. It is located on Navajo land east of Page, Arizona. Antelope Canyon includes two separate, scenic slot canyon sections, referred to individually as Upper Antelope Canyon or The Crack; and Antelope Canyon or The Corkscrew."
      },
      {
        "imageName": "tunnel-of-love-ukraine.jpg",
        "imageUrl": "../../public/assets/images/bamboo-forest-japan.jpg",
        "name": "Bamboo Forest",
        "state": "Japan",
        "posts": [],
        "type": "created by humans",
        "interesting": "canola is rapeseed",
        "links": "http://www.visiontimes.com/2015/03/03/a-gorgeous-sea-of-yellow-the-canola-flower-fields-of-luoping-photos.html?photo=2",
        "description": "Bamboo Forest is a tourist site in Arashiyama, Kyoto, Japan. On CNN, it was referred to as One of the most beautiful groves on Earth. The Ministry of the Environment included the Sagano Bamboo Forest on its list of 100 Soundscapes of Japan."
      },
      {
        "imageName": "hitachi-seaside-park-japan.jpg",
        "imageUrl": "../../public/assets/images/black-forest-germany.jpg",
        "name": "Black Forest",
        "state": "Germany",
        "posts": [],
        "type": "created by humans",
        "interesting": "canola is rapeseed",
        "links": "http://www.visiontimes.com/2015/03/03/a-gorgeous-sea-of-yellow-the-canola-flower-fields-of-luoping-photos.html?photo=2",
        "description": "The Black Forest stretches from the High Rhine in the south to the Kraichgau in the north. In the west it is bounded by the Upper Rhine Plain (which, from a natural region perspective, also includes the low chain of foothills); in the east it transitions to the Gäu, Baar and hill country west of the Klettgau. The Black Forest is the highest part of the South German Scarplands and much of it is densely wooded."
      },
      {
        "imageName": "jackson-hole-wyoming.jpg",
        "imageUrl": "../../public/assets/images/canola-flower-fields-china.jpg",
        "name": "Canola Flower Fields",
        "state": "China",
        "posts": [],
        "type": "created by humans",
        "interesting": "canola is rapeseed",
        "links": "http://www.visiontimes.com/2015/03/03/a-gorgeous-sea-of-yellow-the-canola-flower-fields-of-luoping-photos.html?photo=2",
        "desription": "Opened in October 2016, Anantara Al Jabal Al Akhdar Resort brings the Anantara brand to Oman. Resting on the breathtaking Green Mountain, the resort combines luxury and style with a backdrop of outstanding panoramic views of these fabled mountains."
      },
      {
        "imageName": "jura-france.jpg",
        "imageUrl": "/src/public/assets/images/antelope-canyon-usa.jpg",
        "name": "Antelope Canyon",
        "state": "USA",
        "posts": [],
        "type": "created by hueeeeemans",
        "interesting": "canola is rapeseed",
        "links": "http://www.visiontimes.com/2015/03/03/a-gorgeous-sea-of-yellow-the-canola-flower-fields-of-luoping-photos.html?photo=2",
        "description": "Antelope Canyon is a slot canyon in the American Southwest. It is located on Navajo land east of Page, Arizona. Antelope Canyon includes two separate, scenic slot canyon sections, referred to individually as Upper Antelope Canyon or The Crack; and Antelope Canyon or The Corkscrew."
      },
      {
        "imageName": "lake-hillier-australia.jpg",
        "imageUrl": "../../public/assets/images/bamboo-forest-japan.jpg",
        "name": "Bamboo Forest",
        "state": "Japan",
        "posts": [],
        "type": "created by humans",
        "interesting": "canola is rapeseed",
        "links": "http://www.visiontimes.com/2015/03/03/a-gorgeous-sea-of-yellow-the-canola-flower-fields-of-luoping-photos.html?photo=2",
        "description": "Bamboo Forest is a tourist site in Arashiyama, Kyoto, Japan. On CNN, it was referred to as One of the most beautiful groves on Earth. The Ministry of the Environment included the Sagano Bamboo Forest on its list of 100 Soundscapes of Japan."
      },
      {
        "imageName": "laos-vientiane-buddha-park.jpg",
        "imageUrl": "../../public/assets/images/black-forest-germany.jpg",
        "name": "Black Forest",
        "state": "Germany",
        "posts": [],
        "type": "created by humans",
        "interesting": "canola is rapeseed",
        "links": "http://www.visiontimes.com/2015/03/03/a-gorgeous-sea-of-yellow-the-canola-flower-fields-of-luoping-photos.html?photo=2",
        "description": "The Black Forest stretches from the High Rhine in the south to the Kraichgau in the north. In the west it is bounded by the Upper Rhine Plain (which, from a natural region perspective, also includes the low chain of foothills); in the east it transitions to the Gäu, Baar and hill country west of the Klettgau. The Black Forest is the highest part of the South German Scarplands and much of it is densely wooded."
      },
      {
        "imageName": "lavender-fields-france.jpg",
        "imageUrl": "../../public/assets/images/canola-flower-fields-china.jpg",
        "name": "Canola Flower Fields",
        "state": "China",
        "posts": [],
        "type": "created by humans",
        "interesting": "canola is rapeseed",
        "links": "http://www.visiontimes.com/2015/03/03/a-gorgeous-sea-of-yellow-the-canola-flower-fields-of-luoping-photos.html?photo=2",
        "desription": "Opened in October 2016, Anantara Al Jabal Al Akhdar Resort brings the Anantara brand to Oman. Resting on the breathtaking Green Mountain, the resort combines luxury and style with a backdrop of outstanding panoramic views of these fabled mountains."
      },
      {
        "imageName": "madagascar.jpg",
        "imageUrl": "/src/public/assets/images/antelope-canyon-usa.jpg",
        "name": "Antelope Canyon",
        "state": "USA",
        "posts": [],
        "type": "created by hueeeeemans",
        "interesting": "canola is rapeseed",
        "links": "http://www.visiontimes.com/2015/03/03/a-gorgeous-sea-of-yellow-the-canola-flower-fields-of-luoping-photos.html?photo=2",
        "description": "Antelope Canyon is a slot canyon in the American Southwest. It is located on Navajo land east of Page, Arizona. Antelope Canyon includes two separate, scenic slot canyon sections, referred to individually as Upper Antelope Canyon or The Crack; and Antelope Canyon or The Corkscrew."
      },
      {
        "imageName": "mendenhall-ice-caves-alaska.jpg",
        "imageUrl": "../../public/assets/images/bamboo-forest-japan.jpg",
        "name": "Bamboo Forest",
        "state": "Japan",
        "posts": [],
        "type": "created by humans",
        "interesting": "canola is rapeseed",
        "links": "http://www.visiontimes.com/2015/03/03/a-gorgeous-sea-of-yellow-the-canola-flower-fields-of-luoping-photos.html?photo=2",
        "description": "Bamboo Forest is a tourist site in Arashiyama, Kyoto, Japan. On CNN, it was referred to as One of the most beautiful groves on Earth. The Ministry of the Environment included the Sagano Bamboo Forest on its list of 100 Soundscapes of Japan."
      },
      {
        "imageName": "mount-roraima-venezuela.jpg",
        "imageUrl": "../../public/assets/images/black-forest-germany.jpg",
        "name": "Black Forest",
        "state": "Germany",
        "posts": [],
        "type": "created by humans",
        "interesting": "canola is rapeseed",
        "links": "http://www.visiontimes.com/2015/03/03/a-gorgeous-sea-of-yellow-the-canola-flower-fields-of-luoping-photos.html?photo=2",
        "description": "The Black Forest stretches from the High Rhine in the south to the Kraichgau in the north. In the west it is bounded by the Upper Rhine Plain (which, from a natural region perspective, also includes the low chain of foothills); in the east it transitions to the Gäu, Baar and hill country west of the Klettgau. The Black Forest is the highest part of the South German Scarplands and much of it is densely wooded."
      },
      {
        "imageName": "red-beach-panjin-china.jpg",
        "imageUrl": "../../public/assets/images/bamboo-forest-japan.jpg",
        "name": "Bamboo Forest",
        "state": "Japan",
        "posts": [],
        "type": "created by humans",
        "interesting": "canola is rapeseed",
        "links": "http://www.visiontimes.com/2015/03/03/a-gorgeous-sea-of-yellow-the-canola-flower-fields-of-luoping-photos.html?photo=2",
        "description": "Bamboo Forest is a tourist site in Arashiyama, Kyoto, Japan. On CNN, it was referred to as One of the most beautiful groves on Earth. The Ministry of the Environment included the Sagano Bamboo Forest on its list of 100 Soundscapes of Japan."
      },
      {
        "imageName": "tea-fiels-china.jpg",
        "imageUrl": "../../public/assets/images/black-forest-germany.jpg",
        "name": "Black Forest",
        "state": "Germany",
        "posts": [],
        "type": "created by humans",
        "interesting": "canola is rapeseed",
        "links": "http://www.visiontimes.com/2015/03/03/a-gorgeous-sea-of-yellow-the-canola-flower-fields-of-luoping-photos.html?photo=2",
        "description": "The Black Forest stretches from the High Rhine in the south to the Kraichgau in the north. In the west it is bounded by the Upper Rhine Plain (which, from a natural region perspective, also includes the low chain of foothills); in the east it transitions to the Gäu, Baar and hill country west of the Klettgau. The Black Forest is the highest part of the South German Scarplands and much of it is densely wooded."
      },
      {
        "imageName": "tulip-fields-netherlands.jpg",
        "imageUrl": "../../public/assets/images/black-forest-germany.jpg",
        "name": "Black Forest",
        "state": "Germany",
        "posts": [],
        "type": "created by humans",
        "interesting": "canola is rapeseed",
        "links": "http://www.visiontimes.com/2015/03/03/a-gorgeous-sea-of-yellow-the-canola-flower-fields-of-luoping-photos.html?photo=2",
        "description": "The Black Forest stretches from the High Rhine in the south to the Kraichgau in the north. In the west it is bounded by the Upper Rhine Plain (which, from a natural region perspective, also includes the low chain of foothills); in the east it transitions to the Gäu, Baar and hill country west of the Klettgau. The Black Forest is the highest part of the South German Scarplands and much of it is densely wooded."
      },
      {
        "imageName": "tianzi-mountains-china.jpg",
        "imageUrl": "../../public/assets/images/bamboo-forest-japan.jpg",
        "name": "Bamboo Forest",
        "state": "Japan",
        "posts": [],
        "type": "created by humans",
        "interesting": "canola is rapeseed",
        "links": "http://www.visiontimes.com/2015/03/03/a-gorgeous-sea-of-yellow-the-canola-flower-fields-of-luoping-photos.html?photo=2",
        "description": "Bamboo Forest is a tourist site in Arashiyama, Kyoto, Japan. On CNN, it was referred to as One of the most beautiful groves on Earth. The Ministry of the Environment included the Sagano Bamboo Forest on its list of 100 Soundscapes of Japan."
      },
      {
        "imageName": "tunnel-of-love-ukraine.jpg",
        "imageUrl": "../../public/assets/images/black-forest-germany.jpg",
        "name": "Black Forest",
        "state": "Germany",
        "posts": [],
        "type": "created by humans",
        "interesting": "canola is rapeseed",
        "links": "http://www.visiontimes.com/2015/03/03/a-gorgeous-sea-of-yellow-the-canola-flower-fields-of-luoping-photos.html?photo=2",
        "description": "The Black Forest stretches from the High Rhine in the south to the Kraichgau in the north. In the west it is bounded by the Upper Rhine Plain (which, from a natural region perspective, also includes the low chain of foothills); in the east it transitions to the Gäu, Baar and hill country west of the Klettgau. The Black Forest is the highest part of the South German Scarplands and much of it is densely wooded."
      },
      {
        "imageName": "anantaraOman.jpg",
        "imageUrl": "../../public/assets/images/black-forest-germany.jpg",
        "name": "Black Forest",
        "state": "Germany",
        "posts": [],
        "type": "created by humans",
        "interesting": "canola is rapeseed",
        "links": "http://www.visiontimes.com/2015/03/03/a-gorgeous-sea-of-yellow-the-canola-flower-fields-of-luoping-photos.html?photo=2",
        "description": "The Black Forest stretches from the High Rhine in the south to the Kraichgau in the north. In the west it is bounded by the Upper Rhine Plain (which, from a natural region perspective, also includes the low chain of foothills); in the east it transitions to the Gäu, Baar and hill country west of the Klettgau. The Black Forest is the highest part of the South German Scarplands and much of it is densely wooded."
      }
];

export const loadPlaces = () =>{placeModel.find({}).remove(function() {
    placeModel.collection.insert(places, (err,docs)=>{
    if (err){
      console.log(`failed to Load Place Data`);
    }
    else{
      console.info(`${places.length} places were successfully stored.`);
    }
  })
});
}