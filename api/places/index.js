import express from 'express';
import _ from 'lodash';
import mongoose from 'mongoose';
import Place from './placeModel';
import config from './../../config/config';


const router = express.Router();

//Get places
router.get('/', (req, res) => {
 Place.find((err, places) => {
    if(err) { return handleError(res, err); }
    return res.send(places);
  });
});


//Add a place
router.post('/', (req, res) => {
    console.log('creating a new place: ');
    console.log(req.body);
    let newPlace = req.body;
    if (newPlace){
       Place.create(newPlace, (err, place) => {
           console.log(newPlace);
          if(err) { return handleError(res, err); }
             return res.send({place});
      });
  }else{
       return handleError(res, err);
  }
});


//Update a place
router.put('/:id', (req, res) => {
	 let key = req.params.id;
     let updatePlace = req.body;
     console.log('updating a place');
   if(updatePlace._id) { delete updatePlace._id; }
   Place.findById(req.params.id,  (err, place) => {
      if (err) { return handleError(res, err); }
        if(!place) { return res.send(404); }
            const updated = _.merge(place, req.body);
            updated.save((err) => {
                  if (err) { return handleError(res, err); }
                          return res.send(place);
            });
      });
});

//Delete a place
router.delete('/:id', (req, res) => {
     let key = req.params.id;
     console.log('deleting place', key);
   Place.findById(key, (err, place)=> {
    if(err) { return res.status(400).send(err);}
    if(!place) { return res.send(404); }
    place.remove(err => {
      if(err) { return handleError(res, err); }
      return res.send(place);
    });
  });
});

router.get('/:id', (req, res) => {
  let key = req.params.id;
 Place.findById(key, (err, place) => {
    if(err) { return handleError(res, err); }
    return res.send(place);
    console.log('getting place: ', place)
  });
});

function handleError(res, err) {
  return res.status(500).send(err);
};

export default router;