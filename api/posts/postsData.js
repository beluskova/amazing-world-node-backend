import mongoose from 'mongoose';
import assert from 'assert';
import postModel from './postsModel';
import config from './../../config/config';

const posts =[
    {
        "title": "My new post",
        "body": "<p>Lorem ipsum dolor sit amet, <u>consectetur adipiscing elit</u>. Cras mollis iaculis nisi in lacinia. Aenean ac commodo neque. Duis sollicitudin turpis vel elementum faucibus. Morbi risus leo, cursus in nisi eget, blandit malesuada elit. Integer ut augue dignissim</p>",
        "user": "Homer",
        "date": 1523400891482,
        "summary": "",
      },
      {
        "title": "Hello world!",
        "body": "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. <b>Cras mollis iaculis nisi in lacinia. Aenean ac commodo neque. </b> Duis sollicitudin turpis vel elementum faucibus. Morbi risus leo, cursus in nisi eget, blandit malesuada elit. Integer ut augue dignissim</p>",
        "user": "Sinead",
        "date": 1523405302235,
        "summary": "",

      },
      {
        "title": "WOOOOOO",
        "body": "<p><i>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</i> Cras mollis iaculis nisi in lacinia. <ul>Aenean ac commodo neque.</ul> <ul> Duis sollicitudin turpis vel elementum faucibus.</ul> <ul>Morbi risus leo, cursus in nisi eget, blandit malesuada elit. </ul> Integer ut augue dignissim</p>",
        "user": "Dino",
        "date": 1523405312312,
        "summary": "",

      },
      {
        "title": "HIIIIIII!!!!!",
        "body": "<p>Lorem ipsum <b>dolor</b> sit<p>",
        "user": "Lassie",
        "date": 1523405319145,
        "summary": "",
      },
      {
        "title": "WOOO!!!",
        "body": "<p>Lorem ipsum <b>dolor</b> sit<p>",
        "user": 4,
        "date": 1523456632174,
        "summary": "",
      }

];


export const loadPosts = () =>{postModel.find({}).remove(function() {
    postModel.collection.insert(posts, (err,docs)=>{
    if (err){
      console.log(`failed to Load Post Data`);
    }
    else{
      console.info(`${posts.length} posts were successfully stored.`);
    }
  })
});
}