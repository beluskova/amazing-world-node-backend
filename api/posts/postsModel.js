const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

const PostSchema = new Schema({
    title: String,
    body: String,
    user: String,
    date: String,
    summary: String

});

PostSchema.path('title').validate((v) => {
    if (v.length > 50 || v.length < 5) {
        return false;
    }
    return true;
});

export default mongoose.model('Post', PostSchema);