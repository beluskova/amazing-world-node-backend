import express from 'express';
import _ from 'lodash';
import mongoose from 'mongoose';
import Post from './postsModel';
import config from './../../config/config';


const router = express.Router();

//Get posts
router.get('/', (req, res) => {
 Post.find((err, posts) => {
    if(err) { return handleError(res, err); }
    return res.send(posts);
  });
});


//Add a post
router.post('/', (req, res) => {
    console.log('creating a new post: ');
    console.log(req.body);
    let newPost = req.body;
    if (newPost){
       Post.create(newPost, (err, post) => {
           console.log(newPost);
          if(err) { return handleError(res, err); }
             return res.send({post});
      });
  }else{
       return handleError(res, err);
  }
});


//Update a post
router.put('/:id', (req, res) => {
	 let key = req.params.id;
     let updatePost = req.body;
     console.log('updating a post');
   if(updatePost._id) { delete updatePost._id; }
   Post.findById(req.params.id,  (err, post) => {
      if (err) { return handleError(res, err); }
        if(!post) { return res.send(404); }
            const updated = _.merge(post, req.body);
            updated.save((err) => {
                  if (err) { return handleError(res, err); }
                          return res.send(post);
            });
      });
});

//Delete a post
router.delete('/:id', (req, res) => {
     let key = req.params.id;
     console.log('deleting post', key);
   Post.findById(key, (err, post)=> {
    if(err) { return res.status(400).send(err);}
    if(!post) { return res.send(404); }
    post.remove(err => {
      if(err) { return handleError(res, err); }
      return res.send(post);
    });
  });
});

router.get('/:id', (req, res) => {
  let key = req.params.id;
 Post.findById(key, (err, post) => {
    if(err) { return handleError(res, err); }
    return res.send(post);
    console.log('getting post: ', post)
  });
});

function handleError(res, err) {
  return res.status(500).send(err);
};

export default router;