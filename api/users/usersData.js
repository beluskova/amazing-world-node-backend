import mongoose from 'mongoose';
import assert from 'assert';
import userModel from './userModel';
import config from './../../config/config';

const users =[
 {
  "username": "admin",
  "firstname": "Administrator",
  "lastname": "Clever",
  "email": "admin@admin.com",
  "password": "admin"
},
{
  "username": "homer",
  "firstname": "Homer",
  "lastname": "Simpsonwsasssdsdddd",
  "email": "homer@simpson.com",
  "password": "$2a$10$HeTcw/LI7vhdaf2RF8Cs4u4h8dtNl5hDsvyyYPHgdejvq3hqjjjBq"
} 
];


export const loadUsers = () =>{userModel.find({}).remove(function() {
    userModel.collection.insert(users, (err,docs)=>{
    if (err){
      console.log(`failed to Load User Data`);
    }
    else{
      console.info(`${users.length} users were successfully stored.`);
    }
  })
});
}