import express from 'express';
import _ from 'lodash';
import mongoose from 'mongoose';
import User from './userModel';
import config from './../../config/config';
import asyncHandler from 'express-async-handler';
import jwt from 'jsonwebtoken';
import dotenv from 'dotenv';
import bcrypt from  'bcrypt-nodejs';


const router = express.Router();

/* //Get users
router.get('/', (req, res) => {
 User.find((err, users) => {
    if(err) { return handleError(res, err); }
    return res.send(users);
  });
});
 */

// Get all users, using try/catch to handle errors
router.get('/', async (req, res) => {
  try {
    const users= await User.find();
    res.status(200).json(users);
  } catch (error) {
    handleError(res, error.message);
  }
});

// Register/login a user, using async handler
/* router.post('/', asyncHandler(async (req, res) => {
  console.log('server api user login: ', username, password);
  if (!req.body.username || !req.body.password) {
    res.json({
      success: false,
      msg: 'Please pass username and password.',
    });
  };
  if (req.query.action === 'register') {
    const newUser = new User({
      username: req.body.username,
      password: req.body.password,
    });
    // save the user
    await newUser.save();
    res.status(201).json({
      success: true,
      msg: 'Successful created new user.',
    });
  } else {
    const user = await User.findOne({
      username: req.body.username,
    });
    if (!user) return res.status(401).send({success: false, msg: 'Authentication failed. User not found.'});
    user.comparePassword(req.body.password, (err, isMatch) => {
      if (isMatch && !err) {
        // if user is found and password is right create a token
        const token = jwt.sign(user.username, process.env.secret);
        // return the information including token as JSON
        res.status(200).json({
          success: true,
          token: 'BEARER ' + token,
        });
      } else {
        res.status(401).send({
          success: false,
          msg: 'Authentication failed. Wrong password.',
        });
      }
    });
  };
})); */

 router.post('/',  (req, res) => {
  console.log('server api user login: ');
  console.log(req.body);
  if (!req.body.username || !req.body.password) {
    res.json({
      success: false,
      msg: 'Please pass username and password.',
    });
  };
  if (req.query.action === 'register') {
    const newUser = new User({
      username: req.body.username,
      password: req.body.password,
    });
    // save the user
     newUser.save();
    res.status(201).json({
      success: true,
      msg: 'Successful created new user.',
    });
  } else {
    User.findOne({ username: req.body.username }, function(err, user) {
      if (err) throw err;

    if (!user) return res.status(401).send({success: false, msg: 'Authentication failed. User not found.'});
    console.log('user before compare password: ',user);
    user.comparePassword(req.body.password, (err, isMatch) => {
      console.log('comparing passwords' );
      if (err) throw err;
     // if (isMatch && !err) {
        // if user is found and password is right create a token
        const token = jwt.sign(user.username, process.env.secret);
        // return the information including token as JSON
        console.log('token: ' , token);
      //  localStorage.setItem('user', token);
        res.status(200).json({
          success: true,
          token: 'BEARER ' + token,
        });
    /*   } else {
        console.log('no password match');
        res.status(401).send({
          success: false,
          msg: 'Authentication failed. Wrong password.',
        });
      } */
    });
  });
  };
}); 

/*  //Add a user
router.post('/', (req, res) => {
  console.log('server api user login: ');
    let newUser = req.body;
    console.log('server api user login: ', newUser);
		if (newUser){
           User.create(newUser, (err, user) => {
              if(err) { return handleError(res, err); }
                 return res.send({user});
          });
      }else{
      	 return handleError(res, err);
      }
}); 
 */


//Update a user
router.put('/:id', (req, res) => {
	 let key = req.params.id;
	 let updateUser = req.body;

   if(updateUser._id) { delete updateUser._id; }
   User.findById(req.params.id,  (err, user) => {
      if (err) { return handleError(res, err); }
        if(!user) { return res.send(404); }
            const updated = _.merge(user, req.body);
            updated.save((err) => {
                  if (err) { return handleError(res, err); }
                          return res.send(user);
            });
      });
});

//Delete a user
router.delete('/:id', (req, res) => {
	 let key = req.params.id;
   User.findById(key, (err, user)=> {
    if(err) { return res.status(400).send(err);}
    if(!user) { return res.send(404); }
    user.remove(err => {
      if(err) { return handleError(res, err); }
      return res.send(user);
    });
  });
});

router.get('/:id', (req, res) => {
  let key = req.params.id;
 User.findById(key, (err, user) => {
    if(err) { return handleError(res, err); }
    return res.send(user);
    console.log('getting user profile', user)
  });
});

function handleError(res, err) {
  return res.status(500).send(err);
};

/* let comparePassword = function(passw, cb) {
  console.log(passw);
  bcrypt.compare(passw, this.password, (err, isMatch) => {
      if (err) {
          return cb(err);
      }
      cb(null, isMatch);
  });
};
 */



export default router;