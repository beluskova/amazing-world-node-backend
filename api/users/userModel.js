import bcrypt from 'bcrypt-nodejs';

const mongoose = require('mongoose'),
	Schema = mongoose.Schema;

const UserSchema = new Schema({
	username: String,
	firstname: String,
	lastname: String,
	email: String,
	password: String,
});

UserSchema.path('username').validate((v) => {
	if (v.length > 10 || v.length < 5) {
		return false;
	}
	return true;
});

UserSchema.path('password').validate((v) => {
	if (v.length > 10 || v.length < 5) {
		return false;
	}
	return true;
});

UserSchema.path('email').validate(function (email) {
	var emailRegex = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	return emailRegex.test(email.text); // Assuming email has a text attribute
}, 'The e-mail field cannot be empty.')

UserSchema.pre('save', function (next) {
	const user = this;
	if (this.isModified('password') || this.isNew) {
		bcrypt.genSalt(10, (err, salt) => {
			if (err) {
				return next(err);
			}
			bcrypt.hash(user.password, salt, null, (err, hash) => {
				if (err) {
					return next(err);
				}
				user.password = hash;
				next();
			});
		});
	} else {
		return next();
	}
});

UserSchema.methods.comparePassword = function (passw, cb) {
	bcrypt.compare(passw, this.password, (err, isMatch) => {
		console.log('compare passwords: ', passw, this.password)
		if (err) {
			return cb(err);
		}
		cb(null, isMatch);
	});
};

export default mongoose.model('User', UserSchema);