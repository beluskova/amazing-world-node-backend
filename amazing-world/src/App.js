import React, {Component, PropTypes} from 'react';
import Header from '../src/components/partials/Header';
import {connect} from 'react-redux';
import CSS       from './css/app.less';
import { loginUser} from '../src/actions/ActionsUser';
import Login from '../src/components/users/Login';



class App extends React.Component {

  render() {
 const { dispatch, isAuthenticated, errorMessage } = this.props;
    return (
      <div className="app-container">
        <Header 
                //  isAuthenticated={isAuthenticated}
                //  errorMessage={errorMessage}
                //  dispatch={dispatch}
                />
        <main>
        {this.props.children}
        </main>
      </div>
    );
  }
}

App.propTypes = {
  children: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired,
 // isAuthenticated: PropTypes.bool.isRequired,
 // errorMessage: PropTypes.string,
 
}

function mapStateToProps(state) {
  
  //const { isAuthenticated, errorMessage } = auth
  
  return {
  // isAuthenticated,
  //  errorMessage
  }
}

export default connect(mapStateToProps)(App)

