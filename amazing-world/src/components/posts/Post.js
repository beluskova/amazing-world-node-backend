import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as ActionsPost from '../../actions/ActionsPost';
import { browserHistory } from 'react-router';
import NewPostForm from '../posts/NewPostForm';

class Post extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            post: Object.assign({}, this.props.post),
            saving: false,
            isEditing: false
        };
        this.savepost = this.savePost.bind(this);
        this.updatePostState = this.updatePostState.bind(this);
        this.toggleEdit = this.toggleEdit.bind(this);
        this.deletePost = this.deletePost.bind(this);
        this.redirect = this.redirect.bind(this);
        console.log(this.state);
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.post.id != nextProps.post.id) {
            this.setState({ post: Object.assign({}, nextProps.post) });
        }
        this.setState({ saving: false, isEditing: false });
    }

    toggleEdit() {
        this.setState({ isEditing: true });
    }

    updatePostState(event) {
        const field = event.target.name;
        const post = this.state.post;
        post[field] = event.target.value;
        return this.setState({ post: post });
    }

    savePost(event) {
        event.preventDefault();
        this.setState({ saving: true });
        this.props.actions.updatePost(this.state.post);

    }


    deletePost(event) {
        this.props.actions.deletePost(this.state.post)
        console.log("Post deleted");
    }

    redirect() {
        browserHistory.push('/userHome');
    }

    initQuill (html) {
		if (!this.quill) {
			this.quill = new Quill(this.refs.editor, { 
				theme: 'snow', 
				modules: {
					'link-tooltip': true,
					'image-tooltip': true,
					'toolbar': {
						container: this.refs.toolbar
					}
				}
			}); 
		}
		this.quill.setHTML(html);
    }


    render() {
        const userAdmin = JSON.parse(localStorage.getItem('userAdmin'));
        const user = JSON.parse(localStorage.getItem('user'));

        if (this.state.isEditing) {
            return (
                <div className="basic-input">
                    <h1>Edit post</h1>
                    <NewPostForm
                        post={this.state.post}
                        onSave={this.savePost}
                        onChange={this.updatePostState}
                        saving={this.state.saving} />
                </div>
            )
        }
        return (
            <div className="home">
                <h2> {this.props.post.title}</h2>
                <div className="col-md-5">
                    <p><b> Title: </b> {user.username}</p>
                </div>
                <div className="col-md-12">
                    {userAdmin ?
                        <button type="submit" onClick={this.toggleEdit} className="submit">Edit</button> :
                        user ?
                            <button type="submit" onClick={this.toggleEdit} className="submit">Edit</button> :
                            <p></p>}
                    {userAdmin ?
                        <button type="submit" onClick={this.deletePost} className="submit">Delete</button> :
                        <p></p>}
                </div>
            </div>
        );
    }
}


Post.propTypes = {
    post: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired
};

function getUserById(users, id) {
    const loggedInUser = JSON.parse(localStorage.getItem('user'));
    if (loggedInUser) {
        let user = users.find(user => user.id == loggedInUser.id)
        return Object.assign({}, user)
    }
    else {
        let user = users.find(user => user.id == id)
        return Object.assign({}, user)
    }
}

function getPostById(posts, id) {
    let post = posts.find(post => post.id == id)
    return Object.assign({}, post);
}

function mapStateToProps(state, ownProps) {
    let post = { title: ''};
    const postId = ownProps.params.id;
    if (postId && state.posts.length > 0) {
        post = getPostById(state.posts, ownProps.params.id);
    }
    return { post: post };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(ActionsPost, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Post);







