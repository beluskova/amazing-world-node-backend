import React, {PropTypes} from 'react';
import {Link, browserHistory} from 'react-router';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as ActionsPost from '../../actions/ActionsPost';
import ListPost from './ListPost';
import PostItem from './PostItem';


class ListAllPosts extends React.Component {
  constructor(props, context) {
    super(props, context);
  }




render() {
const userAdmin = JSON.parse(localStorage.getItem('userAdmin'));
const users = this.props.users;
const posts = this.props.posts;

return (
  <div className="app-container">
    <div className="col-md-12">
      <h2><ListPost posts={posts} /></h2>
    </div>
  </div>
);
}
}


ListAllPosts.propTypes = {
  posts: PropTypes.array.isRequired,
  children: PropTypes.object
};

function mapStateToProps(state, ownProps) {
  return {
    posts: state.posts
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(ActionsPost, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ListAllPosts);



