import React, { PropTypes, Component } from 'react';  
import { connect } from 'react-redux';  
import FormInput from '../partials/FormInput';
import ClassNames from 'classnames';
import update       from 'react-addons-update';
import Quill        from 'quill';
import Moment       from 'moment';
import * as courseActions from '../../actions/ActionsPost';
import BasicInput   from '../partials/BasicInput';
import postApi from '../../api/PostApi';
import {bindActionCreators} from 'redux';
import {browserHistory} from 'react-router';


class NewPostForm extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount () {
    var newPostTmpl = '<div>Hello World!</div><div><b>This</b> is my story...</div><div><br/></div>';
    !this.editMode && this.initQuill(newPostTmpl);
}

  initQuill (html) {
    if (!this.quill) {
        this.quill = new Quill(this.refs.editor, { 
            theme: 'snow', 
            modules: {
            //    'link-tooltip': true,
             //   'image-tooltip': true,
                'toolbar': {
                    container: this.refs.toolbar
                }
            }
        }); 
    }
    this.quill.setHTML(html);
}

submit (e) {
  console.log('submit function start');
  var postBody = this.quill.getHTML().replace(/data-reactid="[^"]+"/g, '')
  ,   fullText = this.quill.getText()
  ,   summary  = fullText.slice(0, Config.postSummaryLength);
  var detail = {};

  e.stopPropagation(); 
      e.preventDefault();
      console.log('submit function before if');
  if(errors.length > 0) { 
          console.log('edit post failure');
    this.setState(update(this.state));
    this.getInputEle('title').focus();
  } 
  
  else {
    console.log('update post');
    
    this.props.actions.createPost(post)
         .then(function(post)  {
            setTimeout(function () {
              console.log("SUCCESS", arguments);
              }.bind(this), 2000);
      }.bind(this));
    }
  }

  render() {
    return (
      <div className={ClassNames({ 'error': this.props.error})} {...this.props} >
        <form className="login-form" onSubmit={this.props.onSave}>
        	<fieldset
>
					<FormInput 
						type="text" 
            name="title" 
            placeholder="title"
						value={this.props.post.title}
				//		error={this.state.validity.title}
						onChange={this.props.onChange}
						placeholder="post title"  
					/>
					<hr style={{content:''}} />
					<br/>
					<div className="rich-editor">
						<div ref="toolbar">
							<span className="ql-format-group">
								<select title="Font" className="ql-font" defaultValue="serif">
									<option value="sans-serif" >Sans Serif</option>
									<option value="serif">Serif</option>
									<option value="monospace">Monospace</option>
								</select>
								<select className="ql-size">
									<option value="10px">Small</option>
									<option value="13px">Normal</option>
									<option value="18px">Large</option>
									<option value="32px">Huge</option>
								</select>  
							</span>
							<span className="ql-format-group">
								<span className="ql-bold ql-format-button"></span>
								<span className="ql-format-separator"></span>
								<span className="ql-italic ql-format-button"></span>
								<span className="ql-format-separator"></span>
								<span className="ql-underline ql-format-button"></span>
								<span className="ql-format-separator"></span>
							</span>
							<span className="ql-format-group">
								<select title="Text Color" className="ql-color">
									<option value="rgb(0, 0, 0)" label="rgb(0, 0, 0)"></option>
									<option value="rgb(230, 0, 0)" label="rgb(230, 0, 0)"></option>
									<option value="rgb(255, 153, 0)" label="rgb(255, 153, 0)"></option>
									<option value="rgb(255, 255, 0)" label="rgb(255, 255, 0)"></option>
									<option value="rgb(0, 138, 0)" label="rgb(0, 138, 0)"></option>
									<option value="rgb(0, 102, 204)" label="rgb(0, 102, 204)"></option>
									<option value="rgb(153, 51, 255)" label="rgb(153, 51, 255)"></option>
									<option value="rgb(255, 255, 255)" label="rgb(255, 255, 255)"></option>
									<option value="rgb(250, 204, 204)" label="rgb(250, 204, 204)"></option>
									<option value="rgb(255, 235, 204)" label="rgb(255, 235, 204)"></option>
									<option value="rgb(255, 255, 204)" label="rgb(255, 255, 204)"></option>
									<option value="rgb(204, 232, 204)" label="rgb(204, 232, 204)"></option>
									<option value="rgb(204, 224, 245)" label="rgb(204, 224, 245)"></option>
									<option value="rgb(235, 214, 255)" label="rgb(235, 214, 255)"></option>
									<option value="rgb(187, 187, 187)" label="rgb(187, 187, 187)"></option>
									<option value="rgb(240, 102, 102)" label="rgb(240, 102, 102)"></option>
									<option value="rgb(255, 194, 102)" label="rgb(255, 194, 102)"></option>
									<option value="rgb(255, 255, 102)" label="rgb(255, 255, 102)"></option>
									<option value="rgb(102, 185, 102)" label="rgb(102, 185, 102)"></option>
									<option value="rgb(102, 163, 224)" label="rgb(102, 163, 224)"></option>
									<option value="rgb(194, 133, 255)" label="rgb(194, 133, 255)"></option>
									<option value="rgb(136, 136, 136)" label="rgb(136, 136, 136)"></option>
									<option value="rgb(161, 0, 0)" label="rgb(161, 0, 0)"></option>
									<option value="rgb(178, 107, 0)" label="rgb(178, 107, 0)"></option>
									<option value="rgb(178, 178, 0)" label="rgb(178, 178, 0)"></option>
									<option value="rgb(0, 97, 0)" label="rgb(0, 97, 0)"></option>
									<option value="rgb(0, 71, 178)" label="rgb(0, 71, 178)"></option>
									<option value="rgb(107, 36, 178)" label="rgb(107, 36, 178)"></option>
									<option value="rgb(68, 68, 68)" label="rgb(68, 68, 68)"></option>
									<option value="rgb(92, 0, 0)" label="rgb(92, 0, 0)"></option>
									<option value="rgb(102, 61, 0)" label="rgb(102, 61, 0)"></option>
									<option value="rgb(102, 102, 0)" label="rgb(102, 102, 0)"></option>
									<option value="rgb(0, 55, 0)" label="rgb(0, 55, 0)"></option>
									<option value="rgb(0, 41, 102)" label="rgb(0, 41, 102)"></option>
									<option value="rgb(61, 20, 102)" label="rgb(61, 20, 102)"></option>
								</select>
								<select title="Background Color" className="ql-background">
									<option value="rgb(0, 0, 0)" label="rgb(0, 0, 0)"></option>
									<option value="rgb(230, 0, 0)" label="rgb(230, 0, 0)"></option>
									<option value="rgb(255, 153, 0)" label="rgb(255, 153, 0)"></option>
									<option value="rgb(255, 255, 0)" label="rgb(255, 255, 0)"></option>
									<option value="rgb(0, 138, 0)" label="rgb(0, 138, 0)"></option>
									<option value="rgb(0, 102, 204)" label="rgb(0, 102, 204)"></option>
									<option value="rgb(153, 51, 255)" label="rgb(153, 51, 255)"></option>
									<option value="rgb(255, 255, 255)" label="rgb(255, 255, 255)"></option>
									<option value="rgb(250, 204, 204)" label="rgb(250, 204, 204)"></option>
									<option value="rgb(255, 235, 204)" label="rgb(255, 235, 204)"></option>
									<option value="rgb(255, 255, 204)" label="rgb(255, 255, 204)"></option>
									<option value="rgb(204, 232, 204)" label="rgb(204, 232, 204)"></option>
									<option value="rgb(204, 224, 245)" label="rgb(204, 224, 245)"></option>
									<option value="rgb(235, 214, 255)" label="rgb(235, 214, 255)"></option>
									<option value="rgb(187, 187, 187)" label="rgb(187, 187, 187)"></option>
									<option value="rgb(240, 102, 102)" label="rgb(240, 102, 102)"></option>
									<option value="rgb(255, 194, 102)" label="rgb(255, 194, 102)"></option>
									<option value="rgb(255, 255, 102)" label="rgb(255, 255, 102)"></option>
									<option value="rgb(102, 185, 102)" label="rgb(102, 185, 102)"></option>
									<option value="rgb(102, 163, 224)" label="rgb(102, 163, 224)"></option>
									<option value="rgb(194, 133, 255)" label="rgb(194, 133, 255)"></option>
									<option value="rgb(136, 136, 136)" label="rgb(136, 136, 136)"></option>
									<option value="rgb(161, 0, 0)" label="rgb(161, 0, 0)"></option>
									<option value="rgb(178, 107, 0)" label="rgb(178, 107, 0)"></option>
									<option value="rgb(178, 178, 0)" label="rgb(178, 178, 0)"></option>
									<option value="rgb(0, 97, 0)" label="rgb(0, 97, 0)"></option>
									<option value="rgb(0, 71, 178)" label="rgb(0, 71, 178)"></option>
									<option value="rgb(107, 36, 178)" label="rgb(107, 36, 178)"></option>
									<option value="rgb(68, 68, 68)" label="rgb(68, 68, 68)"></option>
									<option value="rgb(92, 0, 0)" label="rgb(92, 0, 0)"></option>
									<option value="rgb(102, 61, 0)" label="rgb(102, 61, 0)"></option>
									<option value="rgb(102, 102, 0)" label="rgb(102, 102, 0)"></option>
									<option value="rgb(0, 55, 0)" label="rgb(0, 55, 0)"></option>
									<option value="rgb(0, 41, 102)" label="rgb(0, 41, 102)"></option>
									<option value="rgb(61, 20, 102)" label="rgb(61, 20, 102)"></option>
								</select>
							</span>
							<span className="ql-format-group">
								<span title="List" className="ql-format-button ql-list"></span>
								<span className="ql-format-separator"></span>
								<span title="Bullet" className="ql-format-button ql-bullet"></span>
								<span className="ql-format-separator"></span>
								<select title="Text Alignment" className="ql-align">
									<option value="left" label="Left"></option>
									<option value="center" label="Center"></option>
									<option value="right" label="Right"></option>
									<option value="justify" label="Justify"></option>
								</select>
							</span>
							<span className="ql-format-group">
								<span className="ql-link ql-format-button"></span>
								<span className="ql-format-separator"></span>
								<span className="ql-image ql-format-button"></span>
							</span>
						</div>
						<div ref="editor">
						<input 
						 />
						</div>
					</div>
				</fieldset>

          <button type="submit"> submit </button>
        </form>
      </div>
  );
  }
}

NewPostForm.propTypes = {
  post: React.PropTypes.object.isRequired,
  onSave: React.PropTypes.func.isRequired,
  onChange: React.PropTypes.func.isRequired,

};

export default NewPostForm;
