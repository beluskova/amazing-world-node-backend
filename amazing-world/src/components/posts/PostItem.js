import React, {PropTypes} from 'react';
import {Link, browserHistory} from 'react-router';
import Moment       from 'moment';
import ClassNames from 'classnames';
import CSS       from '../../../src/css/components/home.less';
import Auth from './../../auth/auth';
//import Post from './Post';

const dateFormat    = 'MM/DD/YYYY HH:mm:ss';


const user = Auth.getToken;
if (user != null) {
 //user = this.getUserFromPost(post)
 name = 'Homer Simpson';
/*  user.firstName && user.lastName ? 
      user.firstName + ' ' + user.lastName : 
      user.firstName ?
      user.firstName : 
      user.username
  */

const PostItem = ({post}) => {
 

  return (
    <div className="post-view-full">
    <div className="post-view-container">
            <div className="post-metadata">
                <h2><strong>{post.title}</strong> </h2>
                <h5> <span className="user-name">by <i> {post.user}</i> </span> </h5>
                <h6><em>{Moment(post.date, 'x').format(dateFormat)}</em> </h6>
            </div> 
        <ul>    
        <section className="post-body" dangerouslySetInnerHTML={{__html: post.body}}>
        </section>
        { user ? (
						<div>
							<Link to={`/posts/${post.id}/edit`}>
								more...
							</Link>
						</div>
					) : ''
				}
        </ul>
        <br/>
    </div>
</div>
  );
}


PostItem.propTypes = {
  post: PropTypes.object.isRequired
}
}
else {
  console.log("no token => no postItem");
  browserHistory.push('/');
  //return null;
}

//export default PostItem;