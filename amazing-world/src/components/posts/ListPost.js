import React, {PropTypes} from 'react';
import PostItem from './PostItem';



const ListPost = ({posts}) => {  
  return (
      <ul>
        {posts.map(post => 
          <PostItem key={post.id} post={post} />
        )}
    </ul>
  );
};

ListPost.propTypes = {
  posts: PropTypes.array.isRequired
};

export default ListPost;