
import 'react-quill/dist/quill.snow.css';
//import 'react-quill/dist/quill.bubble.css';
import React, { PropTypes, Component } from 'react';  
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {browserHistory} from 'react-router';
//import * as courseActions from '../../actions/ActionsPost';
import NewPostForm from './NewPostForm';
import ClassNames from 'classnames';
import FormInput from '../partials/FormInput';
import BasicInput   from '../partials/BasicInput';
import update       from 'react-addons-update';
import Moment       from 'moment';
import ReactQuill, { Quill, Mixin, Toolbar } from 'react-quill'; 
import * as ActionsPost from '../../actions/ActionsPost';
import Config     from '../../AppConfig';

/* 
 * Simple editor component that takes placeholder text as a prop 
 */
class Editor extends React.Component {
    constructor (props) {
      super(props)
    //  this.state = { post: "" };
      this.state = {
        post: {title: '', body: '', user: 4, date: Moment().valueOf(), summary:''},
        saving: false
      }
      this.redirect = this.redirect.bind(this);
      this.savePost = this.savePost.bind(this);
      this.updatePostState = this.updatePostState.bind(this);
      this.handleChange = this.handleChange.bind(this)
    }

    /*
      componentDidMount () {
        var newPostTmpl = '<div>Hello World!</div><div><b>This</b> is my story...</div><div><br/></div>';
      !this.editMode && this.initQuill(newPostTmpl) 
    }*/
    
      updatePostState(event) {
          console.log('updating post state');
        const field = event.target.name;
        const post = this.state.post;
        post[field] = event.target.value;
        return this.setState({post: post});
      }
    
      redirect(post) {
        browserHistory.push('/userHome');
      }
    
      savePost(event) {
        event.preventDefault();
        this.handleChange();
          console.log('save post function started');
       /* const loggedInUser = JSON.parse(localStorage.getItem('user'));
        var postBody = event.getHTML().replace(/data-reactid="[^"]+"/g, '')
		,   fullText = this.quill.getText()
        ,   summary  = fullText.slice(0, Config.postSummaryLength);*/
        console.log(this.state.post.slice(3,-4));
        this.props.actions.createPost(this.state.post.slice(3,-4)
            //this.state.post
           /* ( 
         //   title= this.state.post.title,
            body= postBody,
            user= loggedInUser.id,
            date= Moment().valueOf()) // unix UTC milliseconds
          //  summary= summary)*/
        ).then((post) => {
             console.log('creating new post');
         // this.redirect(post);
         });
    
      }
      

    handleChange (value) {
        console.log('handlechange');
        this.setState({ post : value });
    }
    
    handleThemeChange (newTheme) {
      if (newTheme === "core") newTheme = null;
      this.setState({ theme: newTheme })
    }
    
    render () {
      return (
        <div>
              <form className="login-form" onSubmit={this.savePost}>
            <fieldset>
              <div>
            <hr style={{content:''}} />
			<br/>

          <ReactQuill 
            name="body" 
            theme={this.state.theme}
            onChange={this.handleChange}
            value={this.state.post}
            modules={Editor.modules}
            formats={Editor.formats}
            placeholder={this.props.placeholder}
          />
           />
          <div className="themeSwitcher">
            <label>Theme </label>
            <select onChange={(e) => 
                this.handleThemeChange(e.target.value)}>
              <option value="snow">Snow</option>
              <option value="bubble">Bubble</option>
              <option value="core">Core</option>
            </select>
          </div>
         </div>
         <button type="submit">Submit</button>
            </fieldset>
      </form>
      </div>
       )
    }
  }
  
  /* 
   * Quill modules to attach to editor
   * See https://quilljs.com/docs/modules/ for complete options
   */
  Editor.modules = {
    toolbar: [
      [{ 'header': '1'}, {'header': '2'}, { 'font': [] }],
      [{size: []}],
      ['bold', 'italic', 'underline', 'strike', 'blockquote'],
      [{'list': 'ordered'}, {'list': 'bullet'}, 
       {'indent': '-1'}, {'indent': '+1'}],
      ['link', 'image', 'video'],
      ['clean']
    ],
    clipboard: {
      // toggle to add extra line breaks when pasting HTML:
      matchVisual: false,
    }
  }
  /* 
   * Quill editor formats
   * See https://quilljs.com/docs/formats/
   */
  Editor.formats = [
    'header', 'font', 'size',
    'bold', 'italic', 'underline', 'strike', 'blockquote',
    'list', 'bullet', 'indent',
    'link', 'image', 'video'
  ]
  
  /* 
   * PropType validation
   */
  Editor.propTypes = {
    placeholder: PropTypes.string,
  }
  
  /* 
   * Render component on page
   */
 /* ReactDOM.render(
    <Editor placeholder={'Write something...'}/>, 
    document.querySelector('.app')
  )*/

 // export default Editor;

 Editor.propTypes = {
    actions: PropTypes.object.isRequired
  };
  
  function mapStateToProps(state, ownProps) {
    return {
      post: state.post
    };
  }
  
  function mapDispatchToProps(dispatch) {
    return {
      actions: bindActionCreators(ActionsPost, dispatch)
    };
  }
  
  
  export default connect(mapStateToProps, mapDispatchToProps)(Editor);