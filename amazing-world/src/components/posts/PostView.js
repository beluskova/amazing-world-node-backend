import React, {Component, PropTypes}      from 'react';
import { Link, browserHistory } from 'react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ClassNames from 'classnames';
import Moment     from 'moment';
import * as courseActions from '../../actions/ActionsPost';


const dateFormat    = 'MM/DD/YYYY HH:mm:ss';
 
//export default React.createClass({
class PostView extends React.Component {
		constructor(props, context) {
		  super(props, context);
		}


	getUserFromPost(post) {
		const users = this.state.users;
		console.log(users);
	//	return users.find (function (user) {
			users.filter(user => {return user.id === post.user});
	//		return user.id === post.user;
	//	});
	}

	getPost () {
		this.props.actions.updatePost(post)
		.then(function (post) {
		}.bind(this));  	
	}

	render () {
		const posts = this.props.posts;
		console.log(posts);
		const post = this.state.post;
		console.log(post);
		    user = JSON.parse(localStorage.getItem('user'));
		  // user = this.getUserFromPost(post)
		   name = user.firstName && user.lastName ? 
				user.firstName + ' ' + user.lastName : 
				user.firstName ?
				user.firstName : 
				user.username
		;
 
		return
		this.props.mode === 'summary' ? (
			// SUMMARY / LIST VIEW
			<li className="post-view-summary">
				<aside>
					<div className="post-metadata">
						<strong>{post.title}</strong>
						<span className="user-name">{name}</span>
						<em>{Moment(post.date, 'x').format(dateFormat)}</em> 
					</div>
				</aside>
				<summary>{post.summary}</summary>
				&nbsp;
				<Link to={`/posts/${post.id}`}>read more</Link> 
				{
					user ? (
						<div>
							<Link to={`/posts/${post.id}/edit`}>
								<button>edit post</button>
							</Link>
						</div>
					) : ''
				}
			</li> 
		) : (
			// FULL POST VIEW
			<div className="post-view-full">
				<div className="post-view-container">
					<h2>
						<div className="post-metadata">
							<strong>{post.title}</strong>
							<span className="user-name">{name}</span>
							<em>{Moment(post.date, 'x').format(dateFormat)}</em> 
						</div> 
					</h2>
					<section className="post-body" dangerouslySetInnerHTML={{__html: post.body}}>
					</section>
				</div>
			</div>

		);
	}
}

PostView.propTypes = {
	posts: PropTypes.array.isRequired,
	children: PropTypes.object
  };
  
  function mapStateToProps(state, ownProps) {
	return {
	  posts: state.posts
	};
  }
  
  function mapDispatchToProps(dispatch) {
	return {
	  actions: bindActionCreators(courseActions, dispatch)
	};
  }
  
  export default connect(mapStateToProps, mapDispatchToProps)(PostView);
 
