import React, { PropTypes, Component } from 'react';  
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {browserHistory} from 'react-router';
import * as courseActions from '../../actions/ActionsPost';
import NewPostForm from './NewPostForm';
import ClassNames from 'classnames';
import FormInput from '../partials/FormInput';
import BasicInput   from '../partials/BasicInput';
import update       from 'react-addons-update';
//import Quill        from 'quill';
import Moment       from 'moment';
import Auth from './../../auth/auth';
//import ReactQuill from 'react-quill'; 
//import * as ActionsPost from '../../actions/ActionsPost';
//import Editor from './Editor';

class AddNewPost extends React.Component {

  constructor(props) {
    super(props);
    const userId = Auth.getToken();
    this.state = {
      post: {title: '', body: '<p>Lorem ipsum <b>dolor</b> sit<p>', user: userId, date: Moment().valueOf(), summary:''},
      saving: false
    };
    this.redirect = this.redirect.bind(this);
    this.savePost = this.savePost.bind(this);
    this.updatePostState = this.updatePostState.bind(this);
  }


  updatePostState(event) {
    console.log('updating post state');
    const loggedInUser  = Auth.getToken();
    const field = event.target.name;
    const post = this.state.post;
    post[field] = event.target.value;
    return this.setState({ post: post });
  }

  redirect(post) {
    browserHistory.push('/userHome');
  }

  savePost(event) {
    event.preventDefault();
    console.log('save post function started');
    const loggedInUser = Auth.getToken();
    this.props.actions.createPost(this.state.post)
        .then(function (result) {
            // go to newly created entry
            console.log('post created');
            browserHistory.push('/userHome');
         //   this.history.pushState('', `/posts/${result.body.id}`);
        }.bind(this));
  }



  render() {
    return (
        <div className="basic-input">
        <fieldset>
            <NewPostForm
              post={this.state.post}
              onSave={this.savePost}
              onChange={this.updatePostState} />
          </fieldset>
           </div>
    );
  }
}

AddNewPost.propTypes = {
    actions: PropTypes.object.isRequired
  };
  
  function mapStateToProps(state, ownProps) {
    return {
      post: state.post
    };
  }
  
  function mapDispatchToProps(dispatch) {
    return {
      actions: bindActionCreators(courseActions, dispatch)
    };
  }
  
  
  export default connect(mapStateToProps, mapDispatchToProps)(AddNewPost);

