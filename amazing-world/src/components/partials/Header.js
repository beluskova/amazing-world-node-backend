import { Link, IndexLink } from 'react-router';
import CSS from '../../css/partials/header.less';
import React, { Component, PropTypes } from 'react'
import Login from '../users/Login'
import Logout from '../users/Logout'
import { loginUser, logoutUser } from '../../actions/ActionsUser';
import LogoutAdmin from '../users/LogoutAdmin';
import Auth from './../../auth/auth';

class Header extends React.Component {

  render() {

    const user  = Auth.getToken();
    console.log(user);
  //  const userAdmin = JSON.parse(localStorage.getItem('userAdmin'));
    const { dispatch, errorMessage } = this.props

    return (
      <div>
        <header className="app-header">
          {user ?
            <Link to="/"><h1> {user.firstname}  {user.lastname}  </h1></Link> :
    /*        userAdmin ?
            <Link to="/"><h1>{userAdmin.username}   {userAdmin.lastname} </h1></Link> : */
            <Link to="/"><h1>Amazing world</h1></Link>}


          <section className="account-ctrl">
          {user ?
           <IndexLink to="/userHome" activeClassName="active">Home</IndexLink>:
 /*          userAdmin ?
           <IndexLink to="/userHome" activeClassName="active">Home</IndexLink>:  */
          <IndexLink to="/" activeClassName="active">Home</IndexLink>
          }
            {" | "}
            {user ?
              <Link to="/places" activeClassName="active">Places</Link> :
 /*               userAdmin ?
                <Link to="/places" activeClassName="active">Places</Link> :  */
                <Link to="/signup" activeClassName="active">Sign up</Link>}
            {" | "}
            {user ?
              <Link to="/logout" activeClassName="active">Log out</Link> :
      /*         userAdmin ?
                <Link to="/logoutAdmin" activeClassName="active">Log out</Link> :  */
                <Link to="/login">Log In</Link>
            }
          </section>
        </header>
      </div>
    );
  }
}

Header.propTypes = {
  dispatch: PropTypes.func.isRequired,
  // isAuthenticated: PropTypes.bool.isRequired,
  errorMessage: PropTypes.string,
}


export default Header;
