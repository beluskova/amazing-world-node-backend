import React, {PropTypes} from 'react';
import {Link, browserHistory} from 'react-router';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as ActionsUser from '../../actions/ActionsUser';
import userApi from '../../api/UserApi';
import ListPost from '../posts/ListPost';
import PostItem from '../posts/PostItem';
import Auth from './../../auth/auth';

class UserHome extends React.Component {

  addPost() {
    browserHistory.push('/newpost');
   }
 
   editDetails() {
   const users = this.props.users;
  // const user = JSON.parse(localStorage.getItem('user'));

    browserHistory.push('/users/:id');
   }

   listAllPosts() {
    browserHistory.push('/postlist');
   }

   listAllUsers() {
    browserHistory.push('/users');
   }


  render() {
     const user = Auth.getToken();
     console.log(user);
    //const userAdmin = JSON.parse(localStorage.getItem('userAdmin'));
    const users = this.props.users;
    console.log(users);
    const posts = this.props.posts;

  {if (user ) {
  return (
    <div>
      <div>
    {user ?
   //  <h1> Welcome {user.username}! </h1> :
     <h1> Welcome ! </h1> :
   //  userAdmin ?
    // <h1> Welcome {userAdmin.username}! </h1> :
     <h1> Welcome on your home page! It should be impossible to reach here!!! </h1> }
      </div>
      <div className="col-md-2">
      {users ? 
      <div>
      <div >
      <button type="submit"  onClick={this.addPost.bind(this)}> Add a new post</button> 
      </div>
      <br/>
      <div>
      <button type="submit"  onClick={this.editDetails.bind(this)}> Edit my details</button>
      </div>
      </div> 
      :
      // userAdmin ?
      // <div>
      // <div>
      // <button type="submit"  onClick={this.listAllPosts.bind(this)}> View all posts</button> 
      // </div>
      // <div>
      // <button type="submit"  onClick={this.listAllUsers.bind(this)}> View all users</button>
      // </div>
      // </div> 
      // :
      <p></p> 
      }
      </div>
      <hr style={{content:''}} />
					<br/>
      <div className="home">
      <div className="col-md-12">
      <div className="app-container">
    <div className="col-md-12">
      <h2><ListPost posts={posts} /></h2>
      {this.props.children} 
    </div>
  </div>
      </div>
    </div>
    </div>
  )} 
  else {
    console.log("no token => no userHome");
    browserHistory.push('/');
    return null;
  }
  }
  }
}


UserHome.propTypes = {
  posts: PropTypes.array.isRequired,
  users: PropTypes.array.isRequired,
  children: PropTypes.object,
  user: PropTypes.object,
  actions: PropTypes.object.isRequired
};

function getUserById(users, id) {
  let user = users.find( user => user.id == id)
  return Object.assign({}, user)
}

function mapStateToProps(state, ownProps) {
  let user = {username: '', fisrtname: '', lastname: '', email: ''};
  const userId = ownProps.params.id;
  if (userId && state.users.length > 0) {
    user = getUserById(state.users, ownProps.params.id);
  } 
    return {user: user};
}
function mapStateToProps(state, ownProps) {
  return {
    users: state.users,
    posts: state.posts
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(ActionsUser, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(UserHome);

