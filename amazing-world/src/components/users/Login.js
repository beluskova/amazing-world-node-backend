import React, { PropTypes } from 'react';
import { History } from 'react-router';
import ClassNames from 'classnames';
import FormInput from '../partials/FormInput';
import PasswordInput from '../partials/FormInput';
import { loginUser } from '../../actions/ActionsUser';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as courseActions from '../../actions/ActionsUser';
import { browserHistory } from 'react-router';
import CSS from '../../css/components/basicInput.less';
import Auth from './../../auth/auth';
import { userInfo } from 'os';

class Login extends React.Component {

  constructor(props) {
    super(props);
    this.logIn = this.logIn.bind(this);


    this.state = {
      username: '',
      password: '',
      submitted: false
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(e) {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  }

  handleSubmit(e) {
    e.preventDefault();

    this.setState({ submitted: true });
    const { username, password } = this.state;
    const { dispatch } = this.props;
    if (username && password) {
      dispatch(userActions.login(username, password));
    }
  }


  logIn(e) {
    var detail = {};

    Array.prototype.forEach.call(
      e.target.querySelectorAll('input'),
      function (v) {
        detail[v.getAttribute('name')] = v.value;
      });
    e.preventDefault();
    e.stopPropagation();
    this.setState({ submitted: true });
    const users = this.props.users;
    console.log(users);
    if (detail.username && detail.password) {
      this.props.actions.loginUser(detail.username, detail.password)
        .then((user) => {
          console.log(user);
          (Auth.authenticateUser(user.token));
          return user;
        });
          browserHistory.push('/userHome');
    }
    else {
      browserHistory.push('/login');
    }
  }


  render() {
    const { username, password, submitted, isAuthenticated } = this.state;
    return (
      <div className={ClassNames({ 'basic-input': true, 'error': this.props.error })} {...this.props} >
        <form className="login-form" onSubmit={this.logIn}>
          <div className="login-box">
            <fieldset>
              <legend>Log In</legend>
              <div>
                <FormInput
                  name="username"
                  type="text"
                  placeholder="username" />
                {submitted && !username &&
                  <p style={{ color: 'red' }}>Username is required </p>
                }
                <PasswordInput
                  name="password"
                  placeholder="password" />
                {submitted && !password &&
                  <p style={{ color: 'red' }}> Password is required </p>}
              </div>
              <button type="submit">Log In</button>
            </fieldset>
          </div>
        </form>
      </div>
    );
  }
}
Login.propTypes = {
  users: PropTypes.array.isRequired,
  user: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired

};

/*
function getUserById(users, id) {
  let user = users.find( user => user.id == id)
  console.log('id: ' , id);
  console.log('user.id: ', user.id);
  return Object.assign({}, user)
}

function mapStateToProps(state, ownProps) {
  let user = {username: '', fisrtname: '', lastname: '', email: ''};
  const userId = ownProps.params.id;
  if (userId && state.users.length > 0) {
    user = getUserById(state.users, ownProps.params.id);
  } 
    return {user: user};
}*/

function mapStateToProps(state, ownProps) {
  return {
    users: state.users
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(courseActions, dispatch)
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(Login);


