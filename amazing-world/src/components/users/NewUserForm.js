import React, {PropTypes} from 'react';
import FormInput from '../partials/FormInput';
import ClassNames from 'classnames';
import CSS from '../../css/components/basicInput.less';

class NewUserForm extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const user = JSON.parse(localStorage.getItem('user'));
    const userAdmin = JSON.parse(localStorage.getItem('userAdmin'));
    return (
      <div className={ClassNames({ 'error': this.props.error})} {...this.props} >
        <form className="login-form" onSubmit={this.props.onSave}>
        {userAdmin ?
        <FormInput
            name="username"
            label="Username:"
            value={this.props.user.username}
            onChange={this.props.onChange}/>:
        user ?    
          <div>
          <p><b>Username: </b>{this.props.user.username} </p> 
          <h1></h1>
          </div>:
          <FormInput
            name="username"
            label="Username:"
            value={this.props.user.username}
            onChange={this.props.onChange}/>
        }
          <FormInput
            name="firstname"
            label="First name:"
            value={this.props.user.firstname}
            onChange={this.props.onChange}/>
          <FormInput
            name="lastname"
            label="Last name:"
            value={this.props.user.lastname}
            onChange={this.props.onChange}/>

          <FormInput
            name="email"
            label="Email:"
            value={this.props.user.email}
            onChange={this.props.onChange}/>
        {userAdmin ?    
        <FormInput
            name="password"
            label="Password:"
            type="password"
            value={this.props.user.password}/> :
        user ?
          <p></p> :
          <FormInput
            name="password"
            label="Password:"
            type="password"
            value={this.props.user.password}
            onChange={this.props.onChange}/>
        }
          <p> </p>
          <button type="submit"> submit </button>
        </form>
      </div>
  );
  }
}

NewUserForm.propTypes = {
  user: React.PropTypes.object.isRequired,
  onSave: React.PropTypes.func.isRequired,
  onChange: React.PropTypes.func.isRequired,

};

export default NewUserForm;
