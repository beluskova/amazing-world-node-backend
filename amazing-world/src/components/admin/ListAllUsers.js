import React, {PropTypes} from 'react';
import {Link, browserHistory} from 'react-router';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as ActionsUser from '../../actions/ActionsUser';
import ListUser from './ListUser';
import UserItem from './UserItem';


class ListAllUsers extends React.Component {
  constructor(props, context) {
    super(props, context);
  }




render() {
const userAdmin = JSON.parse(localStorage.getItem('userAdmin'));
const users = this.props.users;

return (
  <div className="app-container">
    <div className="col-md-4">
      <h2><ListUser users={users} /></h2>
    </div>
    <div className="col-md-8">
      {this.props.children}
    </div>
  </div>
);
}
}


ListAllUsers.propTypes = {
  users: PropTypes.array.isRequired,
  children: PropTypes.object
};

function mapStateToProps(state, ownProps) {
  return {
    users: state.users
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(ActionsUser, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ListAllUsers);



