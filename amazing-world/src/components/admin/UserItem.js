import React, {PropTypes} from 'react';
import {Link} from 'react-router';
import User from './User';

const UserItem = ({user}) => {
  return (
    <tr>
      <td><Link to={'/users/' + user.id}>{user.username}</Link></td>
    </tr>
  );
}

UserItem.propTypes = {
  user: PropTypes.object.isRequired
}

export default UserItem;