import React, {PropTypes} from 'react';
import UserItem from './UserItem';

const ListUser = ({users}) => {
  return (
    <table className="table">
      <thead>
        <tr>
         <th>Users:</th>
        </tr>
      </thead>
      <tbody>
        {users.map(user => 
          <UserItem key={user.id} user={user} />
        )}
      </tbody>
    </table>
  );
};

ListUser.propTypes = {
  users: PropTypes.array.isRequired
};

export default ListUser;
