import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as ActionsUser from '../../actions/ActionsUser';
import { browserHistory } from 'react-router';
import NewUserForm from '../users/NewUserForm';

class User extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            user: Object.assign({}, this.props.user),
            saving: false,
            isEditing: false
        };
        this.saveUser = this.saveUser.bind(this);
        this.updateUserState = this.updateUserState.bind(this);
        this.toggleEdit = this.toggleEdit.bind(this);
        this.deleteUser = this.deleteUser.bind(this);
        this.redirect = this.redirect.bind(this);
        console.log(this.state);
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.user.id != nextProps.user.id) {
            this.setState({ user: Object.assign({}, nextProps.user) });
        }
        this.setState({ saving: false, isEditing: false });
    }

    toggleEdit() {
        this.setState({ isEditing: true });
    }

    updateUserState(event) {
        const field = event.target.name;
        const user = this.state.user;
        user[field] = event.target.value;
        return this.setState({ user: user });
    }

    saveUser(event) {
        event.preventDefault();
        this.setState({ saving: true });
        this.props.actions.updateUser(this.state.user);

    }


    deleteUser(event) {
        this.props.actions.deleteUser(this.state.user)
        console.log("User deleted");
    }

    redirect() {
        browserHistory.push('/users');
    }


    render() {
        const userAdmin = JSON.parse(localStorage.getItem('userAdmin'));
        const user = JSON.parse(localStorage.getItem('user'));

        if (this.state.isEditing) {
            return (
                <div className="basic-input">
                    <h1>Edit account details</h1>
                    <NewUserForm
                        user={this.state.user}
                        onSave={this.saveUser}
                        onChange={this.updateUserState}
                        saving={this.state.saving} />
                </div>
            )
        }
        return (
            <div className="home">
                <h2> {this.props.user.username}</h2>
                <div className="col-md-5">
                    <p><b> First name: </b> {this.props.user.firstname}</p>
                    <p><b> Last name:</b> {this.state.user.lastname}</p>
                    <p><b>Email:</b> {this.state.user.email}</p>
                </div>
                <div className="col-md-12">
                    {userAdmin ?
                        <button type="submit" onClick={this.toggleEdit} className="submit">Edit</button> :
                        user ?
                            <button type="submit" onClick={this.toggleEdit} className="submit">Edit</button> :
                            <p></p>}
                    {userAdmin ?
                        <button type="submit" onClick={this.deleteUser} className="submit">Delete</button> :
                    user ?
                        <button type="submit" onClick={this.toggleEdit} className="submit">Change password</button> :
                        <p></p>}
                </div>
            </div>
        );
    }
}


User.propTypes = {
    user: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired
};

function getUserById(users, id) {
    const loggedInUser = JSON.parse(localStorage.getItem('user'));
    if (loggedInUser)
    {
        let user = users.find(user => user.id == loggedInUser.id)
        return Object.assign({}, user)
    }
    else
    {    
        let user = users.find(user => user.id == id)
        return Object.assign({}, user)
    }
}

function mapStateToProps(state, ownProps) {
    const loggedInUser = JSON.parse(localStorage.getItem('user'));
    let user = { username: '', firstname: '', lastname: '', email: '' };
    const userId = ownProps.params.id;
    if (userId && state.users.length > 0) {
        user = getUserById(state.users, ownProps.params.id);
    }
    return { user: user };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(ActionsUser, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(User);







