import React, {PropTypes} from 'react';
import FormInput from '../partials/FormInput';
import ClassNames from 'classnames';


class NewPlaceForm extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className={ClassNames({ 'error': this.props.error})} {...this.props} >
        <form className="login-form" onSubmit={this.props.onSave}>
          <FormInput
            name="name"
            placeholder="name"
            value={this.props.place.name}
            onChange={this.props.onChange}/>

          <FormInput
            name="state"
            placeholder="state"
            value={this.props.place.state}
            onChange={this.props.onChange}/>

          <FormInput
            name="type"
            placeholder="type"
            value={this.props.place.type}
            onChange={this.props.onChange}/>

          <FormInput
            name="interesting"
            placeholder="interesting facts"
            value={this.props.place.interesting}
            onChange={this.props.onChange}/>

          <FormInput
            name="urls"
            placeholder="links:"
            value={this.props.place.urls}
            onChange={this.props.onChange}/>
          <p> </p>
          <button type="submit"> submit </button>
        </form>
      </div>
  );
  }
}

NewPlaceForm.propTypes = {
  place: React.PropTypes.object.isRequired,
  onSave: React.PropTypes.func.isRequired,
  onChange: React.PropTypes.func.isRequired,

};

export default NewPlaceForm;
