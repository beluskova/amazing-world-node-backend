import React, { PropTypes } from 'react';
import { Link, browserHistory } from 'react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as ActionsPlace from '../../actions/ActionsPlace';
import ListPlaceItems from './ListPlaceItems';
import PlaceItem from './PlaceItem';
import ListPlaceItemPicture from '../places/ListPlaceItemPicture';
import PlaceItemPicture from '../places/PlaceItemPicture';
import CSS       from '../../../src/css/components/home.less';

const Row = ({ name }) => (
  <div className="row">
    <div>{name}</div>
  </div>
);

class ListPlaces extends React.Component {
  constructor(props, context) {
    super(props, context);
  }

handleClick() {
   console.log("redirecting to add a new place");
   browserHistory.push('/newplace');
  }

render() {
const places = this.props.places;

return (
  <div className="home">
    <h1> World is full of beautiful places </h1>
    <div className="col-md-16">
      <ListPlaceItemPicture places={places} />
      {this.props.children}  
      </div>
    </div>
);
}
}


ListPlaces.propTypes = {
  places: PropTypes.array.isRequired,
  children: PropTypes.object
};

function mapStateToProps(state, ownProps) {
  return {
    places: state.places
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(ActionsPlace, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ListPlaces);



