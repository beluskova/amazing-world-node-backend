import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {browserHistory} from 'react-router';
import * as courseActions from '../../actions/ActionsPlace';
import NewPlaceForm from './NewPlaceForm';


class AddNewPlace extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      place: {name: '', state: '', type: '', interesting: '', urls:'',  imageName: "canolaChina.jpg"  },
      saving: false
    };
    this.redirect = this.redirect.bind(this);
    this.savePlace = this.savePlace.bind(this);
    this.updatePlaceState = this.updatePlaceState.bind(this);
  }

  updatePlaceState(event) {
    const field = event.target.name;
    const place = this.state.place;
    place[field] = event.target.value;
    return this.setState({place: place});
  }

  redirect(place) {
    browserHistory.push('/');
  }

  savePlace(event) {
    event.preventDefault();
    console.log('save place: ', this.state.place);
    this.props.actions.createPlace(this.state.place)
     .then((place) => {
      this.redirect(place);
     });

  }
  
  render() {
    return (
      <div className='basic-input'>
        <h1>Place:</h1>
        <NewPlaceForm 
          place={this.state.place} 
          onSave={this.savePlace}
          onChange={this.updatePlaceState}/>
      </div>
    );
  }
}


AddNewPlace.propTypes = {
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state, ownProps) {
  return {
    place: state.place
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(courseActions, dispatch)
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(AddNewPlace);





