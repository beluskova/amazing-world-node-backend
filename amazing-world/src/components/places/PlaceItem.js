import React, {PropTypes, Component} from 'react';
import {Link} from 'react-router';
import Place from './Place';

const PlaceItem = ({place}) => {
  return (
    <tr>
      <td><Link to={'/places/' + place._id}>{place.name}</Link></td>
     <td>{place._id}</td>
    </tr>
  );
}


PlaceItem.propTypes = {
  place: PropTypes.object.isRequired
}

export default PlaceItem;


