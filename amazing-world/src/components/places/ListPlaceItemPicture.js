import React, {PropTypes, List} from 'react';
import PlaceItemPicture from './PlaceItemPicture';
import CSS       from '../../../src/css/components/home.less';

const ListPlaceItemPicture = ({places}) => {

  return (
   <div>
        {places.map(place => 
          <PlaceItemPicture key={place.id} place={place} />
        )}
  </div>
  );
};

ListPlaceItemPicture.propTypes = {
  places: PropTypes.array.isRequired
};

export default ListPlaceItemPicture;

