import React from 'react';
import {render} from 'react-dom';
import { Provider } from 'react-redux';
import { Router, browserHistory } from 'react-router';
import {createStore, applyMiddleware,compose} from 'redux'
import thunk from 'redux-thunk';
import routes from './routes';
import rootReducer from '../src/reducers/index';
import configureStore from './store/configureStore';
import {loadPlaces} from './actions/ActionsPlace';
import {loadUsers} from './actions/ActionsUser';
import {loadPosts} from './actions/ActionsPost';

const store = configureStore();

store.dispatch(loadPlaces());
store.dispatch(loadUsers());
store.dispatch(loadPosts());

render(
	<Provider store={store}>
	<Router history={browserHistory} routes={routes}/>
	</Provider> , document.getElementById('app'));

