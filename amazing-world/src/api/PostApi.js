import axios from 'axios';
import { browserHistory } from 'react-router';


let url = 'http://localhost:8080'

export const getAllPosts = () => {
  return axios.get(url + '/api/posts').then(resp => 
    resp.data);
};

export const updatePostApi = post => {
  return axios.put(url + `/api/posts/${post.id}`)
              .then(resp => resp.data);
};

export const createPostApi = post => {
    return axios.post(url + `/api/posts`)
                .then(resp => resp.data);
};

export const deletePostApi = post => {
    return axios.delete(url + `/api/posts/${post.id}`)
                .then(resp => resp.data);
};

