import axios from 'axios';
import { browserHistory } from 'react-router';
import Auth from './../auth/auth'

let url = 'http://localhost:8080'

export const getAllUsers = () => {
  return axios.get(url + '/api/users')
  .then(resp => resp.data);
};


export const createUserApi = user => {
  //TODO
};

export const login = (username, password) => {
  console.log('api user login: ', username, password);
  return axios.post(url + '/api/users/', { username: username, password: password })
  .then(resp => resp.data);
};
export const logoutApi = () => {
  Auth.deauthenticateUser();
};

export const logoutAdminApi = () => {
  //TODO
};

export const signup =  user => {
  console.log('api user signup: ', user);
  return axios.post(url + '/api/users?action=register', user)
  .then(resp => resp.data);
};

/* export const signup =  user => {
  console.log('api user signup: ', user);
  return axios.post(url + '/api/users', user)
  .then(resp => resp.data);
}; */