import axios from 'axios';
import { browserHistory } from 'react-router';


let url = 'http://localhost:8080'

export const getAllPlaces = () => {
  return axios.get(url + '/api/places/').then(resp => 
    resp.data);
};

export const updatePlaceApi = place => {
    console.log('update place api: place: ', place);
    console.log(url + `/api/places/${place._id}`);
  return axios.put(url + `/api/places/${place._id}`, place)
              .then(resp => resp.data);
};

export const createPlaceApi = place => {
    console.log('creating a new place: ', place);
    return axios.post(url + `/api/places/`, place)
                .then(resp => resp.data);
};

export const deletePlaceApi = place => {
    return axios.delete(url + `/api/places/${place._id}`)
                .then(resp => resp.data);
};

