
class PlacesApi {
  static getAllPlaces() {
    return fetch('api/places').then(response => {
      return response.json();
    }).catch(error => {
      return error;
    });
  }

  static updatePlace(place) {
    const request = new Request(`api/places/${place.id}`, {
      method: 'PUT',
      headers: new Headers({
        'Content-Type': 'application/json'
      }), 
      body: JSON.stringify(place)
    });


    return fetch(request).then(response => {
      return response.json();
    }).catch(error => {
      return error;
    });
  }

  static createPlace(place) {
    const request = new Request('api/places', {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json'
      }), 
      body: JSON.stringify(place)
    });


    return fetch(request).then(response => {
      return response.json();
    }).catch(error => {
      return error;
    });
  }

  static deletePlace(place) {
    const request = new Request(`api/${place.id}`, {
      method: 'DELETE'
    });

    return fetch(request).then(response => {
      return response.json();
    }).catch(error => {
      return error;
    });
  }
}

export default PlacesApi;
