import * as types from './ActionTypes';
import {createPostApi, deletePostApi, updatePostApi, getAllPosts} from '../api/PostApi';

export function loadPostsSuccess(posts) {
  return {type: types.LOAD_POSTS_SUCCESS, posts};
}

export function updatePostSuccess(post) {
  return {type: types.UPDATE_POST_SUCCESS, post}
}

export function createPostSuccess(post) {
  return {type: types.CREATE_POST_SUCCESS, post}
}

export function deletePostSuccess(post) {
  return {type: types.DELETE_POST_SUCCESS, post}
}

export function loadPosts() {
  return function(dispatch) {
    return getAllPosts().then(posts => {
      dispatch(loadPostsSuccess(posts));
    }).catch(error => {
      throw(error);
    });
  };
}

export function updatePost(post) {
  return function (dispatch) {
    return updatePostApi(post).then(responsePost => {
      dispatch(updatePostSuccess(responsePost));
    }).catch(error => {
      throw(error);
    });
  };
}

export function createPost(post) {
  return function (dispatch) {
      console.log('create post action fired');
    return createPostApi(post).then(responsePost => {
      dispatch(createPostSuccess(responsePost));
      return responsePost;
    }).catch(error => {
      throw(error);
    });
  };
}

export function deletePost(post) {
  return function(dispatch) {
    return deletePostApi(post).then(() => {
      dispatch(deletePostSuccess(post));
      return;
    }).catch(error => {
      throw(error);
    })
  }
}







