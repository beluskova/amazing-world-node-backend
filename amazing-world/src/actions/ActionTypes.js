export const LOAD_PLACES_SUCCESS    = 'LOAD_PLACES_SUCCESS';
export const UPDATE_PLACE_SUCCESS   = 'UPDATE_PLACE_SUCCESS';
export const CREATE_PLACE_SUCCESS   = 'CREATE_PLACE_SUCCESS';
export const DELETE_PLACE_SUCCESS   = 'DELETE_PLACE_SUCCESS';

export const CREATE_USER_SUCCESS    = 'CREATE_USER_SUCCESS';
export const LOAD_USERS_SUCCESS     = 'LOAD_USERS_SUCCESS';
export const UPDATE_USER_SUCCESS    = 'UPDATE_USER_SUCCESS';
export const DELETE_USER_SUCCESS    = 'DELETE_USER_SUCCESS';

export const CREATE_POST_SUCCESS    = 'CREATE_POST_SUCCESS';
export const LOAD_POSTS_SUCCESS     = 'LOAD_POSTS_SUCCESS';
export const UPDATE_POST_SUCCESS    = 'UPDATE_POST_SUCCESS';
export const DELETE_POST_SUCCESS    = 'DELETE_POST_SUCCESS';

export const LOGIN_REQUEST         = 'LOGIN_REQUEST';
export const LOGIN_USER_SUCCESS    = 'LOGIN_USER_SUCCESS';
export const LOGIN_FAILURE         = 'LOGIN_FAILURE';

export const LOGIN_SUCCESS         = 'LOGIN_SUCCESS';
export const LOGOUT_REQUEST        = 'LOGOUT_REQUEST';
export const LOGOUT_SUCCESS        = 'LOGOUT_SUCCESS';
export const LOGOUT_FAILURE        = 'LOGOUT_FAILURE';

export const ALERT_SUCCESS         = 'ALERT_SUCCESS';
export const ALERT_ERROR           = 'ALERT_ERROR';
export const ALERT_CLEAR           = 'ALERT_CLEAR';