import { browserHistory } from 'react-router';
import * as types from './ActionTypes';
import userApi, { getUserApi } from '../api/UserApi';
import { combineReducers } from 'redux';
import {getAllUsers, updateUserApi, createUserApi, login,logoutApi, logoutAdminApi, signup } from '../api/UserApi';
import authenticateUser from './../auth/auth';

export function createUserSuccess(user) {
  return { type: types.CREATE_USER_SUCCESS, user }
}

export function updateUserSuccess(user) {
  return { type: types.UPDATE_USER_SUCCESS, user }
}


export function loadUsersSuccess(users) {

  return { type: types.LOAD_USERS_SUCCESS, users };
}

export function loginRequest(user) {
  return { type: types.LOGIN_REQUEST, user }
}

export function loginSuccess(user) {
  return { type: types.LOGIN_SUCCESS, user }
}

export function loginFailure(error) {
  return { type: types.LOGIN_FAILURE, error }
}

export function logoutSuccess() {
  return { type: types.LOGOUT_SUCCESS }
}

export function alertSuccess(message) {
  return { type: types.ALERT_SUCCESS, message };
}

export function alertError(message) {
  return { type: types.ALERT_ERROR, message };
}

export function alertClear() {
  return { type: types.ALERT_CLEAR };
}

export function loadUsers() {
  return function (dispatch) {
     return getAllUsers().then(users => {
       console.log(users);
     dispatch(loadUsersSuccess(users));
    }).catch(error => {
      throw (error);
    });
  };
}

export function updateUser(user) {
  return function (dispatch) {
    return updateUserApi(user).then(responseUser => {
      dispatch(updateUserSuccess(responseUser));
    }).catch(error => {
      throw(error);
    });
  };
}

export function createUser(user) {
  console.log('action user signup: ', user);
  return function (dispatch) {
    return signup(user).then(responseUser => {
    //  dispatch(createUserSuccess(responseUser));
    //  browserHistory.push('/userHome');
   /*  getAllUsers().then((user) => {
      browserHistory.push('/userHome');
      }); */
      return responseUser;
    }).catch(error => {
      throw (error);
    });
  };
}

export function loginUser(username, password) {
  return function (dispatch) {
    console.log('action user login: ', username, password);
    return login(username, password)
      .then((user) => {
        dispatch(loginSuccess(user));
/*        getAllUsers().then((user) => {
        browserHistory.push('/userHome');
        }); */
        return user;
      }).catch(error => {
        browserHistory.push('/login');
        throw (error);
      });
  }
}

export function logout() {
  return function (dispatch) {
    dispatch(logoutSuccess())
    return logoutApi();
  }
}

export function logoutAdmin() {
  return function (dispatch) {
    dispatch(logoutSuccess())
    return logoutAdminApi();
  }
}

export function removeLocalUsers() {
    return userApi.removeLocalUsers()
    }
