import * as types from './ActionTypes';
import {createPlaceApi, deletePlaceApi, updatePlaceApi, getAllPlaces} from '../api/PlacesApi';

export function loadPlacesSuccess(places) {
  return {type: types.LOAD_PLACES_SUCCESS, places};
}

export function updatePlaceSuccess(place) {
  return {type: types.UPDATE_PLACE_SUCCESS, place}
}

export function createPlaceSuccess(place) {
  return {type: types.CREATE_PLACE_SUCCESS, place}
}

export function deletePlaceSuccess(place) {
  return {type: types.DELETE_PLACE_SUCCESS, place}
}


export function loadPlaces() {
  return function(dispatch) {
    return getAllPlaces().then(places => {
      dispatch(loadPlacesSuccess(places));
    }).catch(error => {
      throw(error);
    });
  };
}

export function updatePlace(place) {
  return function (dispatch) {
    console.log('action update place: ', place);
    return updatePlaceApi(place).then(responsePlace => {
      dispatch(updatePlaceSuccess(responsePlace));
    }).catch(error => {
      throw(error);
    });
  };
}

export function createPlace(place) {
  return function (dispatch) {
    return createPlaceApi(place).then(responsePlace => {
      dispatch(createPlaceSuccess(responsePlace));
      return responsePlace;
    }).catch(error => {
      throw(error);
    });
  };
}

export function deletePlace(place) {
  return function(dispatch) {
    return deletePlaceApi(place).then(() => {
      dispatch(deletePlaceSuccess(place));
      return;
    }).catch(error => {
      throw(error);
    })
  }
}







