import * as types from '../actions/ActionTypes';
import initialState from './InitialState';
import {browserHistory} from 'react-router';


export default function PostReducer(state = initialState.posts, action) {
  switch(action.type) {
    case types.LOAD_POSTS_SUCCESS:
      return action.posts.map(post => Object.assign({}, post))
     return Object.assign([], state, action.posts)
    case types.CREATE_POST_SUCCESS:
     // browserHistory.push('/userHome')
     console.log('create post success');
      return [
        ...state.filter(post => post.id !== action.post.id),
        Object.assign({}, action.post)
      ]
    case types.UPDATE_POST_SUCCESS:
      return [
        ...state.filter(post => post.id !== action.post.id),
        Object.assign({}, action.post)
      ]
    case types.DELETE_POST_SUCCESS: {
      const newState = Object.assign([], state);
      const indexOfPOstToDelete = state.findIndex(post => {return post.id == action.post.id})
      newState.splice(indexOfPostToDelete, 1);
      browserHistory.push('/userHome');
      return newState;
    }
    default: 
      return state;
  }
}
