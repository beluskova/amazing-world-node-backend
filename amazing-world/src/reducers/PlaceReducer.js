import * as types from '../actions/ActionTypes';
import initialState from './InitialState';
import {browserHistory} from 'react-router';


export default function PlaceReducer(state = initialState.places, action) {
  switch(action.type) {
    case types.LOAD_PLACES_SUCCESS:
      return action.places.map(place => Object.assign({}, place))
     return Object.assign([], state, action.places)
    case types.CREATE_PLACE_SUCCESS:
      browserHistory.push('/places')
      return [
        ...state.filter(place => place.id !== action.place.id),
        Object.assign({}, action.place)
      ]
    case types.UPDATE_PLACE_SUCCESS:
      return [
        ...state.filter(place => place.id !== action.place.id),
        Object.assign({}, action.place)
      ]
    case types.DELETE_PLACE_SUCCESS: {
      const newState = Object.assign([], state);
      const indexOfPlaceToDelete = state.findIndex(place => {return place.id == action.place.id})
      newState.splice(indexOfPlaceToDelete, 1);
      //browserHistory.push('/');
      return newState;
    }
    default: 
      return state;
  }
}
