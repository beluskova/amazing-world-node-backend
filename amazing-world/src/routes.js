import React, {Component} from 'react';
import { Route, IndexRoute } from 'react-router';
import App from './App';
import Home from './components/home/Home';
import ListPlaces  from  './components/places/ListPlaces'; 
import Place  from  './components/places/Place'; 
import AddNewPlace  from  './components/places/AddNewPlace'; 
import Signup from './components/users/Register';
import User from './components/admin/User';
import UserHome from './components/users/UserHome';
import Login from './components/users/Login';
import Logout from './components/users/Logout';
import LogoutAdmin from './components/users/LogoutAdmin';
import ListAllUsers  from  './components/admin/ListAllUsers'; 
import AddNewPost  from  './components/posts/AddNewPost'; 
import Editor  from  './components/posts/Editor'; 
import PostView  from  './components/posts/PostView'; 
import PostList  from  './components/posts/ListAllPosts'; 

export default (

  <Route path="/" component={App}>
    <IndexRoute component={Home} />
    <Route 
        path="/places"
        component={ListPlaces} />
    <Route 
        path="/places/:id" 
        component={Place} />
    <Route 
        path="/newplace" 
        component={AddNewPlace} />
    <Route 
        path="/login" 
        component={Login} />

     <Route 
        path="/signup" 
        component={Signup} />

    <Route 
        path="/logout" 
        component={Logout} />

    <Route 
        path="/logoutAdmin" 
        component={LogoutAdmin} />
        
    <Route 
        path="/userHome"
        component={UserHome} />
    <Route 
        path="/users"
        component={ListAllUsers} />
    <Route 
        path="/users/:id" 
        component={User} />

     <Route 
        path="/newpost" 
        component={AddNewPost} />

    <Route 
        path="/editor" 
        component={Editor} />

     <Route 
        path="/postview" 
        component={PostView} />

     <Route 
        path="/postlist" 
        component={PostList} />


  </Route>
);


