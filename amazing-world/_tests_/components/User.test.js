import React from 'react';
import ReactDOM from 'react-dom';
import Header from '../../src/components/partials/Header';
import { mount } from 'enzyme';

describe('Renderin user home', () => {
    it('should be able to run tests', () => {
        expect(1 + 2).toEqual(3);
    });
    
    it('renders without crashing', () => {
        mount(<Header/>);
    }); 
});