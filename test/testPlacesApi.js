import supertest from 'supertest';
import { server } from './../server.js';
import should from 'should'; // eslint-disable-line



// UNIT test begin
describe('Places API unit test', function () {
    this.timeout(120000);

    // #1 return a collection of json documents
    it('should return collection of JSON documents', function (done) {
        // calling home page api
        supertest(server)
            .get('/api/places')
            .expect('Content-type', /json/)
            .expect(200) // This is the HTTP response
            .end(function (err, res) {
                // HTTP status should be 200
                res.status.should.equal(200);
                done();
            });
    });
});


// #2 add a place
describe('Test add place', function () {
    this.timeout(120000);
    it('should add a new place', function (done) {
        setTimeout(function () {
            // post to /api/places
            supertest(server)
                .post('/api/places')
                .send({ name: 'Place 123' })
                .expect('Content-type', /json/)
                .expect(201)
                .end(function (err, res) {
                    res.status.should.equal(200);
                    res.body.should.have.property('_id');
                    res.body.name.should.equal('Place 123');
                    done();
                });
            done();
        }, 1900);
    });
});

// #3 update a place
describe('Test update a place', function () {
    this.timeout(120000);
    it('should UPDATE a place given the id', (done) => {
        const superserver = supertest(server);
        superserver
            .get('/api/places')
            .expect('Content-type', /json/)
            .expect(200)
            .end(function (err, res) {
                setTimeout(function () {
                    const id = res.body[0]._id;
                    superserver
                    supertest(server)
                        .put('/api/places' + id)
                        .send({ name: "Testing put function", state: "USA" })
                        .end((err, res) => {
                            res.status.should.equal(200);
                            res.body.should.have.property('_id');
                            res.body.name.should.equal('Testing put function');
                            res.body.state.should.equal('USA');
                            done();
                        });
                    done();
                }, 1500);
            });
    });
});



// #4 delete a place
describe('Test delete place', function () {
    this.timeout(120000);
    it('should delete place', function (done) {
        const superserver = supertest(server);
        superserver
            .get('/api/places')
            .expect('Content-type', /json/)
            .expect(200) // This is HTTP response
            .end(function (err, res) {
                setTimeout(function () {
                    const id = res.body[0]._id;
                    superserver
                        .put('/api/places/' + id)
                        .expect('Content-type', /json/)
                        .expect(200) // This is HTTP response
                        .end(function (err, res) {
                            res.status.should.equal(200);
                       //     done();
                        });
                    done();
                }, 1500);
            }
            );
    });
});


