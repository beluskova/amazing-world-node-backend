import supertest from 'supertest';
import {server} from './../server.js';
import should from 'should'; // eslint-disable-line


  // #1 return a collection of json documents
describe('Get all users API unit test', function() {
  this.timeout(120000); // eslint-disable-line
  it('should return collection of JSON documents', function(done) {
    // calling home page api
    supertest(server)
    .get('/api/users')
    .expect('Content-type', /json/)
    .expect(200) // This is the HTTP response
    .end(function(err, res) {
        // HTTP status should be 200
        res.status.should.equal(200);
        done();
    });
  });
});

  // #2 add a user
  describe('Register a user Unit test', function() {
    this.timeout(120000); // eslint-disable-line
  it('should register a user', function(done) {
    supertest(server)
    .post('/api/users')
    .query({action: 'register'})
    .send({username: 'Contact 99', password: 'test1'})
    .expect('Content-type', /json/)
    .expect(201)
    .end(function(err, res) {
      res.status.should.equal(201);
      res.body.success.should.equal(true);
      done();
    });
  });
  });

  // #3 login a user
  describe('Authenticate user API unit test', function() {
    this.timeout(120000); // eslint-disable-line
  it('should authenticate a user', function(done) {
    setTimeout(function () {
    supertest(server)
    .post('/api/users')
    .send({username: 'user1', password: 'test1'})
    .expect('Content-type', /json/)
    .expect(201)
    .end(function(err, res) {
      res.status.should.equal(200);
      res.body.token.substring(0, 3).should.equal('JWT');
      done();
    });
    done();
  }, 1999);
  });
});